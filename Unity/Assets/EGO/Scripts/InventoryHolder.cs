﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EGO {

	public class InventoryHolder : MonoBehaviour {

		private List<InventoryItem> _inv = new List<InventoryItem>();
		private InventoryItem _selected = null;

		private UiManager _ui;

		// Use this for initialization
		void Awake () {
		
			_ui = GameObject.FindObjectOfType<UiManager> ();

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void Save() {

		}

		public void Restore() {

		}

		public void ClearSelect() {

			if (_selected!=null) {

				_selected = null;
				_ui.UpdateInventoryUI ();
			}
		}

		public void SelectItem(int ndx) {

			if (ndx >= 0 && ndx < _inv.Count) {

				SelectItem(_inv[ndx]);
			}
		}

		public void ToggleSelectItem(int ndx) {
			
			if (ndx >= 0 && ndx < _inv.Count) {
				
				if (_inv[ndx] == GetSelected()) {

					ClearSelect();
				}
				else {

					InventoryItem itm = GetSelected();
					if (itm!=null) {
						// check for crafting
						if (itm.CanCraft(_inv[ndx])) {
							itm.Craft(_inv[ndx]);
						}
					}
					else {
						SelectItem(_inv[ndx]);
					}
				}
			}
		}

		public void SelectItem(InventoryItem itm) {

			_selected = null;

			if (CheckInventory(itm)) {

				_selected = itm;
			}

			_ui.UpdateInventoryUI ();
		}

		public InventoryItem GetSelected() {

			return _selected;
		}

		public bool CheckInventory(InventoryItem itm) {

			return _inv.Contains (itm);
		}

		public List<InventoryItem> GetInventoryList() {

			return _inv;
		}

		public void AddToInventory(InventoryItem itm) {
			
			_inv.Add (itm);
			_ui.UpdateInventoryUI ();
		}

		public void ReplaceInInventory(InventoryItem oldItm, InventoryItem newItm) {
			
			int pos = -1;
			foreach (InventoryItem itm in _inv) {

				if (itm == oldItm) {
					pos++;
					break;
				}
				pos++;
			}

			//int pos = _inv.BinarySearch (oldItm);
			if (pos>=0) {
				_inv.RemoveAt(pos);
				_inv.Insert(pos, newItm);
			}
			else {
				AddToInventory(newItm);
			}
			_ui.UpdateInventoryUI ();

		}
		public void DeleteFromInventory(InventoryItem itm) {
			
			Debug.Log ("Delete from inventory" + itm);
			_inv.Remove (itm);
			_ui.UpdateInventoryUI ();

		}

	}
}

