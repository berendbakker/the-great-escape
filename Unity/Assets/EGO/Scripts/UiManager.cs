﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;


namespace EGO
{

	public class UiManager : MonoBehaviour {

		private GameObject uiDirBottom;
		private GameObject uiDirTop;
		private GameObject uiDirLeft;
		private GameObject uiDirRight;

		private UnityEngine.UI.Button[] _invButtons = new UnityEngine.UI.Button[3];
		private InventoryHolder _inv;

		// Use this for initialization
		void Awake () {

			uiDirBottom = GameObject.Find ("EgoUiDirBottom");
			uiDirTop = GameObject.Find ("EgoUiDirTop");
			uiDirLeft = GameObject.Find ("EgoUiDirLeft");
			uiDirRight = GameObject.Find ("EgoUiDirRight");
			HideAllDirections ();

			for (int i=0; i<_invButtons.Length; i++) {

				GameObject go = GameObject.Find ("EgoInv" + i);
				if (go) {
					UnityEngine.UI.Button b = go.GetComponent<UnityEngine.UI.Button>();
					if (b) {
						_invButtons[i] = b;
						go.SetActive(false);
					}
				}
			}

			_inv = GameObject.FindObjectOfType<InventoryHolder> ();

			//UpdateInventoryUI ();
		}
		

		private ObjectInteraction iBottom;
		private BehaviorTree btBottom;

		private ObjectInteraction iTop;
		private BehaviorTree btTop;

		private ObjectInteraction iLeft;
		private BehaviorTree btLeft;

		private ObjectInteraction iRight;
		private BehaviorTree btRight;


		public void UpdateInventoryUI() {


			for (int b=0; b<_invButtons.Length; b++) {
				
				_invButtons[b].gameObject.SetActive(false);
			}

			List<InventoryItem> lst = _inv.GetInventoryList ();

			int i = 0;
			foreach (InventoryItem itm in lst) {

				if (itm.showInUI) {
					UnityEngine.UI.Image img = _invButtons[i].GetComponent<UnityEngine.UI.Image>();
					if (img) {
						if (itm == _inv.GetSelected() && itm.iconSelected!=null) {
							img.sprite = itm.iconSelected;
						}
						else {
							img.sprite = itm.icon;
						}
						img.gameObject.SetActive(true);
					}
				}
				i++;
				if (i >= _invButtons.Length) break;
			}
		}

		public void HideAllDirections() {

			uiDirBottom.SetActive (false);
			uiDirTop.SetActive (false);
			uiDirLeft.SetActive (false);
			uiDirRight.SetActive (false);
		}

		public void ShowDirectionBottom(ObjectInteraction i) {

			uiDirBottom.SetActive (true);
			iBottom = i;
			btBottom = null;
		}
		public void ShowDirectionBottom(BehaviorTree bt) {
			
			uiDirBottom.SetActive (true);
			btBottom = bt;
			iBottom = null;
		}

		public void OnClickDirectionBottom() {

			if (iBottom != null) {
				iBottom.Interact(null, new Vector3(0, 0, 0));
			}
			if (btBottom != null) {
				btBottom.EnableBehavior();
			}
		}

		public void ShowDirectionTop(ObjectInteraction i) {
			
			uiDirTop.SetActive (true);
			iTop = i;
			btTop = null;
		}
		public void ShowDirectionTop(BehaviorTree bt) {
			
			uiDirTop.SetActive (true);
			btTop = bt;
			iTop = null;
		}
		
		public void OnClickDirectionTop() {
			
			if (iTop != null) {
				iTop.Interact(null, new Vector3(0, 0, 0));
			}
			if (btTop != null) {
				btTop.EnableBehavior();
			}
		}

		public void ShowDirectionLeft(ObjectInteraction i) {
			
			uiDirLeft.SetActive (true);
			iLeft = i;
			btLeft = null;
		}
		public void ShowDirectionLeft(BehaviorTree bt) {
			
			uiDirLeft.SetActive (true);
			btLeft = bt;
			iLeft = null;
		}
		
		public void OnClickDirectionLeft() {
			
			if (iLeft != null) {
				iLeft.Interact(null, new Vector3(0, 0, 0));
			}
			if (btLeft != null) {
				btLeft.EnableBehavior();
			}
		}

		public void ShowDirectionRight(ObjectInteraction i) {
			
			uiDirRight.SetActive (true);
			iRight = i;
			btRight = null;
		}
		public void ShowDirectionRight(BehaviorTree bt) {
			
			uiDirRight.SetActive (true);
			btRight = bt;
			iRight = null;
		}
		
		public void OnClickDirectionRight() {
			
			if (iRight != null) {
				iRight.Interact(null, new Vector3(0, 0, 0));
			}
			if (btRight != null) {
				btRight.EnableBehavior();
			}
		}

	}
}
