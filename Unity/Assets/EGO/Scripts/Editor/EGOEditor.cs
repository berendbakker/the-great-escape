using UnityEngine;
using UnityEditor;
using System.IO;
using EGO;

public class EGOEditor : EditorWindow
{
	
	public static string version = "0.9.0";
	
	[MenuItem ("EGO/Delete Player Prefs")]
	static void DeletePlayerPrefs ()
	{
		PlayerPrefs.DeleteAll ();
		Debug.Log ("PLAYER PREFS DELETED!");
	}
	
	

}