
#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.IO;

namespace EGO
{
	// for future use

	public static class CustomAssetUtility
	{
		
		public static string GetUniqueAssetPathNameOrFallback (string filename)
		{
			string path;
			try
			{
				System.Type assetdatabase = typeof (UnityEditor.AssetDatabase);
				path = (string) assetdatabase.GetMethod ("GetUniquePathNameAtSelectedPath", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static).Invoke(assetdatabase, new object[] { filename });
			}
			catch
			{
				path = UnityEditor.AssetDatabase.GenerateUniqueAssetPath("Assets/" + filename);
			}
			return path;
		}
		

		public static T CreateAsset<T> () where T : ScriptableObject
		{
			T asset = ScriptableObject.CreateInstance<T> ();
			string assetPathAndName = GetUniqueAssetPathNameOrFallback ("New " + typeof(T).ToString() + ".asset");
			
			AssetDatabase.CreateAsset (asset, assetPathAndName);

			AssetDatabase.SaveAssets ();
			EditorUtility.FocusProjectWindow ();
			Selection.activeObject = asset;

			return asset;
		}
		
		
		public static T CreateAsset<T> (string filename, string path) where T : ScriptableObject
		{
			T asset = ScriptableObject.CreateInstance<T> ();
			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath ("Assets" + Path.DirectorySeparatorChar.ToString () + path + Path.DirectorySeparatorChar.ToString () + filename + ".asset");
			AssetDatabase.CreateAsset (asset, assetPathAndName);
			
			AssetDatabase.SaveAssets ();
			EditorUtility.FocusProjectWindow ();

			return asset;
		}
		

		public static T CreateAndReturnAsset<T> (string path) where T : ScriptableObject
		{
			T asset = ScriptableObject.CreateInstance<T> ();
			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath ("Assets" + Path.DirectorySeparatorChar.ToString () + path + Path.DirectorySeparatorChar.ToString () + typeof(T).ToString() + ".asset");
			AssetDatabase.CreateAsset (asset, assetPathAndName);
			
			AssetDatabase.SaveAssets ();
			EditorUtility.FocusProjectWindow ();
			
			return asset;
		}

	}
}

#endif