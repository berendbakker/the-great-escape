﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace EGO
{

	public class GameManager : MonoBehaviour {

		public float startTimescale = 1f;
		public string mainMenu;

		public static GameManager instance;

		private CameraManager _camMan;

		private bool blockAllGameInput = false;



		// Use this for initialization
		void Awake() {

			instance = this;
			_camMan = FindObjectOfType<CameraManager> ();
			Time.timeScale = startTimescale;

		}

		void OnDestroy() {

			instance = null;
		}

		// Update is called once per frame
		void Update () {
		
		}

		public void BlockAllGameInput(bool block) {

			blockAllGameInput = block;
		}

		public bool IsAllGameInputBlocked() {

			return blockAllGameInput || Time.timeScale == 0f;
		}

		public CameraManager GetCameraManager() {
			
			return _camMan;
		}


		public void RestartLevel() {
			
			Application.LoadLevel (Application.loadedLevelName);
		}

		public void ExitToMenu() {
			
			Application.LoadLevel (mainMenu);
		}


	}

}
