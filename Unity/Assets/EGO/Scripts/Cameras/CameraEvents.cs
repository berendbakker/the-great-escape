﻿using UnityEngine;
using System.Collections;
using HeavyDutyInspector;

namespace EGO
{
	public class CameraEvents : MonoBehaviour {

		public bool clearUiOnEnter = true;
		public bool clearUiOnLeave = true;

		public GameObject onEnterBehavior;
		public GameObject onLeaveBehavior;

		public bool autoToggleInteraction;

		[HideConditional("autoToggleInteraction", true)]
		public Collider interactionCollider;

		private UiManager _ui;

		void Start() {

			_ui = GameObject.FindObjectOfType<UiManager> ();
			if (_ui==null) Debug.LogError("No UiManager found!");
		}


		public virtual void OnEnter() {

			Debug.Log ("Camera OnEnter");

			if (clearUiOnEnter) {
				_ui.HideAllDirections();
			}

			if (interactionCollider!=null) {
				interactionCollider.enabled = false;
			}

			if (onEnterBehavior!=null) {

				onEnterBehavior.SendMessage("EnableBehavior", SendMessageOptions.DontRequireReceiver);
			}
		}

		public virtual void OnLeave() {

			Debug.Log ("Camera OnLeave");

			if (clearUiOnLeave) {
				_ui.HideAllDirections();
			}

			if (interactionCollider!=null) {
				interactionCollider.enabled = true;
			}

			if (onLeaveBehavior!=null) {
				
				onLeaveBehavior.SendMessage("EnableBehavior", SendMessageOptions.DontRequireReceiver);
			}

		}
	}
}