﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace EGO
{

	public class CameraManager : MonoBehaviour {


		public Camera currentCamera;
		private Camera transisionCamera;
		public bool disableNonCurrent = true;

		private Camera mainCamera;
		private Camera prevCamera;

		private bool isSwitching = false;
		private Camera[] _cams;

		public event CamSwitchEvent OnSwitchComplete;
		public delegate void CamSwitchEvent();

		// Use this for initialization
		void Awake () {
		
			// some basic position inits for fading
			//gameObject.transform.position = Vector3.zero;
			//gameObject.transform.localScale = new Vector3(100, 100, 100);

			_cams = FindObjectsOfType<Camera> ();

			foreach (Camera c in _cams) {

				if (currentCamera!=null) {

					mainCamera = currentCamera;
					if (c != currentCamera) {
						if (disableNonCurrent) _EnableCam(c, false);	// c.gameObject.SetActive(false);
					}
				}
				else {
					if (c.tag == "MainCamera") {
						currentCamera = c;
						mainCamera = currentCamera;
						CameraEvents ce = mainCamera.GetComponent<CameraEvents>();
						if (ce) {
							ce.OnEnter();
						}
					}
					else {
						if (disableNonCurrent) _EnableCam(c, false);	// c.gameObject.SetActive(false);
					}
				}
			}

			// create transition camera (if no default provided)
			if (transisionCamera==null) {

				GameObject go = new GameObject();
				transisionCamera = go.AddComponent<Camera>();
				if (mainCamera.GetComponent<AudioListener>()!=null) {
					go.AddComponent<AudioListener>();
				}
				if (mainCamera.GetComponent<GUILayer>()!=null) {
					go.AddComponent<GUILayer>();
				}
				//transisionCamera = Instantiate (mainCamera) as Camera;
				transisionCamera.name = "_transitionCam";
				transisionCamera.tag = null;
				transisionCamera.transform.parent = transform;
				transisionCamera.gameObject.SetActive (false);
			}

		}

		public void SwitchToMainCam(float time = 0f, bool ignoreTimescale = false) {

			if (currentCamera==mainCamera) {
				_Complete(mainCamera);
				return;
			}

			_SwitchToCam (mainCamera, time, ignoreTimescale);
		}

		public void SwitchToPrevCam(float time = 0f, bool ignoreTimescale = false) {

			if (prevCamera==null) {
				_Complete(currentCamera);
			}

			_SwitchToCam (prevCamera, time, ignoreTimescale);
		}

		public void SwitchToCam(Camera cam) {

			SwitchToCam (cam, 0f);
		}

		public void SwitchToCam(Camera cam, float time) {

			_SwitchToCam (cam, time, false);
		}

		public void SwitchToCam(Camera cam, float time, bool ignoreTimescale) {
			
			_SwitchToCam (cam, time, ignoreTimescale);
		}

		public void CancelSwitch() {

			if (_twMove!=null) {
				_twMove.Kill (false);
				_twFov.Kill (false);
				_twRotate.Kill (false);
				_twMove = null;
				_twFov = null;
				_twRotate = null;
			}
			transisionCamera.gameObject.SetActive (false);
			isSwitching = false;
		}

		private void _CopyCam(Camera source, Camera dest) {

			dest.transform.position = source.transform.position;
			dest.transform.rotation = source.transform.rotation;
			dest.aspect = source.aspect;
			dest.depth = source.depth;
			dest.farClipPlane = source.farClipPlane;
			dest.fieldOfView = source.fieldOfView;
			dest.hdr = source.hdr;
			dest.orthographic = source.orthographic;
			dest.nearClipPlane = source.nearClipPlane;
			dest.useOcclusionCulling = source.useOcclusionCulling;
			dest.backgroundColor = source.backgroundColor;
		}

		private void _InstantSwitch(Camera cam) {

			_EnableCam (cam, true);
			CameraEvents ce = cam.GetComponent<CameraEvents> ();
			if (ce) {
				ce.OnEnter();
			}
			//cam.gameObject.SetActive(true);
			_EnableCam (currentCamera, false);
			//currentCamera.gameObject.SetActive(false);
			prevCamera = currentCamera;
			currentCamera = cam;

		}

		Tweener _twMove;
		Tweener _twFov;
		Tweener _twRotate;

		private void _SwitchToCam(Camera cam, float transitionTime, bool ignoreTimescale) {

			if (isSwitching) {
				// interupting a current switch
				CancelSwitch();
			}

			if (transitionTime<=0f) {
				// instance transition
				_InstantSwitch(cam);
				return;
			}

			isSwitching = true;

			_CopyCam (currentCamera, transisionCamera);

			Vector3 dest = cam.transform.position;
			Vector3 rot = cam.transform.eulerAngles;

			transisionCamera.gameObject.SetActive (true);

			CameraEvents ce = currentCamera.GetComponent<CameraEvents> ();
			if (ce) {
				ce.OnLeave();
			}
			_EnableCam (currentCamera, false);
			//currentCamera.gameObject.SetActive (false);
			_twMove = transisionCamera.transform.DOMove (dest, transitionTime);
			_twMove.SetUpdate (ignoreTimescale);
			_twFov = DOTween.To(()=> transisionCamera.fieldOfView, x=> transisionCamera.fieldOfView = x, cam.fieldOfView, transitionTime);
			_twFov.SetUpdate (ignoreTimescale);
			_twRotate = transisionCamera.transform.DORotate (rot, transitionTime);
			_twRotate.SetUpdate (ignoreTimescale);

			_twMove.OnComplete(() => _Complete(cam));
		}

		private void _Complete(Camera cam) {

			_InstantSwitch (cam);
			transisionCamera.gameObject.SetActive (false);
			isSwitching = false;
			if (OnSwitchComplete != null) {

				OnSwitchComplete();
			}
		}

		private void _EnableCam(Camera c, bool enabled) {

			c.enabled = enabled;
			GUILayer gl = c.gameObject.GetComponent<GUILayer> ();
			if (gl!=null) gl.enabled = enabled;
			AudioListener al = c.gameObject.GetComponent<AudioListener> ();
			if (al!=null) al.enabled = enabled;
		}

		public bool IsSwitching() {

			return isSwitching;
		}
	}
}
