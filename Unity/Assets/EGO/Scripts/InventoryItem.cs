﻿using UnityEngine;
using System.Collections;
using HeavyDutyInspector;

namespace EGO {

	public class InventoryItem : MonoBehaviour {

		public bool showInUI = true;
		public bool startsInInventory = false;
		public Sprite icon;
		public Sprite iconSelected;

		public bool canCraft = false;
		[HideConditional("canCraft", true)]
		public InventoryItem craftWith;
		[HideConditional("canCraft", true)]
		public InventoryItem craftResult;

		private InventoryHolder _holder;

		// Use this for initialization
		void Start () {
		
			_holder = GameObject.FindObjectOfType<InventoryHolder> ();

			if (startsInInventory) {
				_holder.AddToInventory(this);
			}
		}
		
		public bool CanCraft(InventoryItem itm) {

			if (canCraft) {
				if (itm == craftWith) {
					return true;
				}
			}

			return itm.CanCraft (this);
		}

		public bool Craft(InventoryItem itm) {

			if (CanCraft (itm)) {

				if (itm == craftWith) {
					_holder.ReplaceInInventory(this, craftResult);
					_holder.DeleteFromInventory(itm);
					_holder.ClearSelect();
					return true;
				}
			}

			return itm.Craft(this);
		}

/*		public void OnChangeOwner(InventoryHolder h) {

			_holder = h;
		}

		public InventoryHolder GetOwner() {

			return _holder;
		}
*/
	}
}

