﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;

namespace EGO
{

	public class ObjectInteraction : MonoBehaviour {

		public BehaviorTree onInteractBT;


		private GameObject _target;
		private GameObject _source;
		private Vector3 _hitPos;
		private bool isBusy = false;

		// Use this for initialization
		void Awake () {
		
			_target = gameObject;


		}
		
		public GameObject GetTarget() {

			return _target;
		}

		public GameObject GetSource() {
			
			return _source;
		}

		public bool IsBusy() {
			
			return isBusy;
		}

		public bool Check(GameObject source, Vector3 hitPos) {

			return OnCheck (source, hitPos);
		}

		public void Interact(GameObject source, Vector3 hitPos) {

			StartCoroutine (InteractCoroutine (source, hitPos));
		}

		private IEnumerator InteractCoroutine(GameObject source, Vector3 hitPos) {

			_source = source;
			_hitPos = hitPos;

			isBusy = true;

			if (onInteractBT!=null) onInteractBT.EnableBehavior();

			if (isBusy) {
				yield return StartCoroutine(OnInteract (source, hitPos));
			}

			if (onInteractBT!=null) {
				while (isBusy && onInteractBT.isActiveAndEnabled) {
					yield return new WaitForEndOfFrame();
				}
			}

			isBusy = false;
		}

		public void StopInteraction() {

			if (onInteractBT != null) onInteractBT.DisableBehavior();
			isBusy = false;
			OnStop ();	// alow implementation to stop
		}

		// implement the interaction
		protected IEnumerator StartDuringBehavior() {

			yield return new WaitForEndOfFrame ();

			if (onInteractBT!=null && isBusy) {
				onInteractBT.EnableBehavior();
			}

			yield return null;
		}


		// implement logic for aborting the interaction
		protected virtual void OnStop() {
			
		}

		// implement check if interaction can be executed
		protected virtual bool OnCheck(GameObject source, Vector3 hitPos) {

			return true;
		}

		// implement the interaction
		protected virtual IEnumerator OnInteract(GameObject source, Vector3 hitPos) {

			yield return null;
			//return null;
		}
	}
}
