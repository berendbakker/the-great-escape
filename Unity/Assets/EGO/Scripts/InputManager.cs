﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace EGO
{
		public class InputManager : MonoBehaviour {
		
		public LayerMask layerRaycast;

		private EventSystem eventsystem;
		private float lastClickTime;

		private GameManager _gm;
		private CameraManager _cm;



		void Start() {

			_gm = FindObjectOfType<GameManager> ();
			_cm = FindObjectOfType<CameraManager> ();
			eventsystem = GameObject.FindObjectOfType<EventSystem> ();
		}

		// Update is called once per frame
		void Update () {

			if (_gm.IsAllGameInputBlocked() || _cm.IsSwitching()) return;

			if(Input.GetMouseButtonDown(0)){
				
				OnTap();
			}
		}
		
		private void OnTap() {
			

			Debug.Log ("OnTap...");

			if (!IsInputOverUI()) {

				Camera c = _cm.currentCamera;

				RaycastHit hitInfo = new RaycastHit();
				if (Physics.Raycast(c.ScreenPointToRay(Input.mousePosition), out hitInfo, Mathf.Infinity, layerRaycast))
				{

					ObjectInteraction[] ocs = FindInteractions(hitInfo);
					if (ocs!=null && ocs.Length > 0) {
						
						Debug.Log ("   ... Interaction found: " + ocs[0]);
						// same interaction object
						ocs[0].Interact(null, hitInfo.point);
					}
				}
			}
		}
		

		private bool IsInputOverUI() {

			// mouse
			if (eventsystem.IsPointerOverGameObject ()) return true;

			// touch
			foreach (Touch touch in Input.touches)
			{
				int pointerID = touch.fingerId;
				if (EventSystem.current.IsPointerOverGameObject(pointerID))
				{
					// at least on touch is over a canvas UI
					return true;
				}
			}

			return false;
		}
		

		private ObjectInteraction[] FindInteractions(RaycastHit hitInfo) {
			
			Transform t = hitInfo.transform;
			
			while (t != null) {
				
				ObjectInteraction[] ocs = t.gameObject.GetComponents<ObjectInteraction> ();
				if (ocs!=null) {
					return ocs;
				}
				t = t.parent;
			}
			
			return null;
		}
	}

}
