-------------------------------------------------------------------------------------------------
                                        Advanced Builder
                                         Version 1.2.6
                                       PygmyMonkey Tools
                                     tools@pygmymonkey.com
                   http://www.pygmymonkey.com/tools/advancedbuilder/index.html
-------------------------------------------------------------------------------------------------

Thank you for buying Advanced Builder!

If you have questions, suggestions, comments or feature requests, please send us an email
at tools@pygmymonkey.com



-------------------------------------------------------------------------------------------------
                            Support, Documentation, Examples and FAQ
-------------------------------------------------------------------------------------------------

You can find everything at http://www.pygmymonkey.com/tools/advancedbuilder/index.html



-------------------------------------------------------------------------------------------------
                                           Get Started
-------------------------------------------------------------------------------------------------

This tool allows you to easily build different "versions" of your game on multiple platforms.
We're going to rapidly describe the different parts of the tool and use some examples.
You can launch the interface by going to "Window/Advanced Builder"


-------------------------------------- Product Parameters ---------------------------------------
Here you will define the parameters for your product.
Currently you can only specify the bundle version, but more will be added in future releases.
Thanks to AdvancedBuilder, you can access the bundle version of your product during gameplay,
using: "AppParameters.Get.bundleVersion" (something you can't do with the current Unity API).


----------------------------------------- Release Types -----------------------------------------
Let's say your game have a Free and a Paid version (or Demo and Full).
Before, you would have to change the bundle identifier, product name and more before each build,
and, if you needed to know which version you were on during runtime, you would have to set (and
update) a boolean somewhere in your code to store this information.
Now, you can just add two release types, define the bundle identifier and product name for each,
click on build and you will have two executables for your product.
Then, during runtime, you can retrieve the release type using "AppParameters.Get.releaseType".


------------------------------------------- Platforms -------------------------------------------
In this section, you can add all the platforms your product supports, and also select some
per-platform parameters such as:

--- Distribution platforms ---
This is very useful if you want to publish your product to different sub-platforms:
- For Android, you may want to deliver your product through Google Play, but also for the Amazon
Appstore and maybe the Samsung AppStore. Well, you just have to add these 3 distribution platforms
and Advanced Builder will automatically build 3 different builds for each of these sub-platforms.
This is handy if you want to support In-App purchase for example, because for that, you need to
call the IAP process from GooglePlay if users downloaded from the Google Play, and same thing
for the Amazon Store. As there is no way of determining if a user downloaded your product through
Google Play or Amazon Store, you can simply use 2 different APKs, and using the variable
AppParameters.Get.distributionPlatform, you can call the IAP method from the correct platform.
- For the Web, you may want to have different builds, for your own website and for Kongregate.
- Same thing for Windows, maybe you want a version that you will sell on your website, and another
that you will sell on Steam (and do different things in the code with it).

--- Textures Compression ---
(used by Android and BlackBerry)
If you use 'Generic', it will build using the default texture compression.
Let's take Android for example. If you want, you can use different texture compressions that will
generate multiple APKs, that you will upload to Google. The Play Store will then automatically
deliver the correct APK depending on the device downloading your product. With this, users will
have an optimized version with texture compressions adapted to their device.

--- Architectures ---
Using this you can simultaneously build for :
- Windows x86 and/or Windows x64
- OSX x86, OSX x64 and/or OSX Universal
- Linux x86, Linux x86_64 and/or Linux Universal
- WebPlayer and/or WebPlayer Streamed
- D3D11 C++, D3D11 C#, XAML C++ and/or XAML C# (for Windows Store 8)
- Local and/or Signed (for BlackBerry)


--------------------------------------- Advanced Settings ---------------------------------------
--- Build folder path ---
Here you can define the destination of your builds.
By default it will be in /Builds/BUILD_DATE/RELEASE_TYPE/PLATFORM/DISTRIB_PLATFORM/ARCHITECTURE/,
some examples:
	/Builds/13-12-16 10h23/Dev/iOS/MyProduct/
	/Builds/13-12-16 10h23/Dev/Android/Google Play/MyProduct - Generic.apk
	/Builds/13-12-16 10h23/Dev/Android/Amazon App Store/MyProduct - Generic.apk
	/Builds/13-12-16 10h23/Beta/Windows/Windows x86/MyProduct.exe
You can personalize the build destination by modifying the build folder path and using, or not,
the different parameters at your disposal :
- $BUILD_DATE
- $RELEASE_TYPE
- $PLATFORM
- $DISTRIB_PLATFORM
- $ARCHITECTURE

--- Custom build ---
You can create a custom script that allow you to do actions before and after each build.
This script needs to implement the interface IAdvancedCustomBuild and be placed in an Editor
folder. You will have to implement the two methods OnPreBuild and OnPostBuild.
These two methods have different parameters that will tell you information on the build that is
currently processing, allowing you to do different things based on the platform, the release type,
the distribution platform, the platform architecture and the texture compression.
You can find examples of custom build scripts in "Example/Editor/".

--- Build options ---
You will find here different default build options such as "Open build folder after a build",
"Autorun build", "Autoconnect profiler" etc...


------------------------------------- Project Configuration -------------------------------------
This section will list all the configurations available based on what you used in 'Release Types',
and 'Platforms' windows.
It is very useful if you want to test a configuration of your game directly in the Editor.
Please note that we do not switch platform when you select a configuration, but only update the
'AppParameters.asset' asset with the configuration. So everything in your code relying on this
script will work, but using scripting define symbols as "#if UNITY_ANDROID" will work only if you
switch platform yourself. So be sure to test everything in your code using the parameters in
AppParameters so you can test how your game will react on different configurations without the
need to switch platform.
Of course, when you'll press the build button, each build will have the configuration you've set.

Using Unity Free:
You can use this window to manually select the configuration you want to use before manually
building each version of your game. So you'll have to click on the 'Set' button to set the
configuration you want, and then build manually every configuration. At the end of each build,
you just select another configuration and build manually again etc...


----------------------------------------- Perform Build -----------------------------------------
In this section, you will find a summary of all the different builds you're about to perform.
Before building, make sure to check the 'Warnings & Errors' section to see if everything is ok...


--------------------------------------- Warnings & Errors ---------------------------------------
This section will tell you if something is wrong. So pay attention before building!



-------------------------------------------------------------------------------------------------
                                            Examples
-------------------------------------------------------------------------------------------------

------------------------------------------- Example 1 -------------------------------------------
Let's say you have a game that you want to release for Android, iOS and OSX Universal.
And you want to have:
- a dev version, so you can test your game without erasing the release one on a device (because
of the same package name)
- a beta version, that you will send to some people
- a release version, that will be released to the public

1. Release types
You will just add 3 release types (dev, beta and release) each having a different bundle
identifier so you can have the 3 different apps on Android & iOS.

2. Platforms
Then you will add Android, iOS and Mac platforms.

3. Architecture
For the Mac platform, you will only select OSX Universal.

You will now have a total of 9 builds ready to go:
- Dev: Android, iOS & OSX
- Beta: Android, iOS & OSX
- Release: Android, iOS & OSX

When your game is ready, just click on the "Perform a total of 9 builds on 3 platforms" button.
That's it!

BONUS
At runtime, you can then use "if (AppParameters.Get.releaseType.Equals("Dev"))" to only
call some debug methods, or send some stats to your webserver.

Don't forget you can use the custom build script, to do really anything before and/or after each
build.


------------------------------------------- Example 2 -------------------------------------------
Now let's say you're making a game for iOS only and you want a Demo and Full version.

1. Release types
You will just add 2 release types (demo and release) each having a different bundle identifier

2. Platforms
Then you will add only the iOS platform.

You will now have a total of 2 builds ready to go:
- Demo: iOS
- Full: iOS

When your game is ready, just click on the Perform a total of 2 builds on 1 platform button.
That's it!

BONUS
At runtime, you can then use "if (AppParameters.Get.releaseType.Equals("Full"))"
to allow users to access different parts of your game.

If you're using a custom build script, you could also, before each build, remove some scenes from
the BuildSettings in the OnPreBuild method if the release type is "Demo", change/remove some
GameObjects from a scene, or anything you want...


-------------------------------------------------------------------------------------------------
                                          Release Notes
-------------------------------------------------------------------------------------------------
1.2.6
- NEW: Added a custom build example showing you how you can chose different scenes depending on
the release type you're building
- NEW: Added a custom build example that will automatically set your Android keystore password

1.2.5
- FIX: Updated documentation
- NEW: Improved the way 'Project Configuration' works, so you can easily try multiple configuration
directly in the Unity Editor without switching platforms.
This update will probably create some errors in your scripts if you were using the AppParameters
class and/or custom build scripts.
To access data via AppParameters, you will now need to call "AppParameters.Get.DATA" where DATA
is the data you want to retrieve (for example, to retrieve the release type, you would call:
AppParameters.Get.releaseType).
For custom build scripts, the OnPreBuild/OnPostBuild methods now take strings instead of enums
as parameters.

1.2.2
- FIX: Fix setting configurations using Unity Free

1.2.1
- NEW: In the Advanced Settings section, you can now chose to display an error when you forgot
to set your Android Keystore and Alias passwords.
- FIX: Display errors when incorrect name format are set for Distribution Platform and Release
Types

1.2.0
- NEW: Added support for Distribution Platforms! (see Get Started at the top for more info)

1.1.1 f1
- FIX: Building for Mac on a Windows machine was disabled by mistake (it's now possible again)

1.1.1
- NEW: Added a 'Project Configuration' window. You can now apply different configuration directly
in the Editor to test your game on each of these configurations. This is also useful for people
using Unity Free. (see Get Started & FAQ for more info).
- NEW: You can now activate/deactivate platforms you want to support (instead of deleting them)
- NEW: Added warning message if you try to build for iOS on Windows or Linux
- NEW: Added error message if using the same name in different release types
- FIX: Scenes not enabled in the build settings will not be build anymore
- FIX: Custom build methods now use the enums in AppParameters.cs instead of classes

1.1.0 f1
- FIX: Fixed BlackBerry build issue (Unity bug if you have a space in the .bar file name)
- FIX: Fixed InvalidOperationException thrown by Unity Editor GUI after a build operation
- FIX: Fixed iOS build if you had the Facebook Unity plugin in your project

1.1.0
- NEW: Added support for Windows Phone 8
- NEW: Added support for Windows Store 8
- NEW: Added support for BlackBery

1.0.1
- FIX: Display warning and errors even when platforms or release types are not fold out
- FIX: Build date on the build folder was incorrect if the total builds took more than 1 minute

1.0.0
- NEW: Initial release


-------------------------------------------------------------------------------------------------
                                          Future Updates
-------------------------------------------------------------------------------------------------

- Tutorial videos -
We will release 1-2 tutorial videos showing how to do specific things with explanations

- Per platform parameters -
Add the possibility to specify some parameters that will change depending on your release type
and/or platform. This can be use for example to define a different AdmobID or FacebookID (or
anything) if you're building the Free or Pro version of your game etc...

- Automated builds -
We will add an easy to use bash script that will automatically launch every x hours and perform
builds for you in the background. For example, you could have your computer starting the build
process every day at midnight, updating the repository from your master branch on
git/mercurial/svn before building. With that, you could have builds for your game ready every day
without having to do anything!

- Build report tool -
Have a report generated after each build where you can see a lot of usefull information on your
build. We will also probably add a custom window that will display all these information with
ease of reading.

- Build verbose -
Add the possibility to add verbose when builds are done, or even have your computer say something
when the build is done (Mac only) etc...

- Version Updater -
Add a simple class that will allows you to check if a user updated the game or installed it for
the first time. You could use that to make database migrations between versions for examples,
or give coins to users that had an issue with an old version of your game etc...

- Custom icons -
Allow you to add a small banner on your icon depending on the release type. This is usefull is
you want to build a free version of the game for example and you want to add the banner "Free"
on the icon. Instead of manually updating the icon before building each version of your game,
this will be done automatically!

- AndroidManifest and info.plist -
Add helper methods that will allow you to easily modify things in your AndroidManifest (Android)
and/or info.plist (iOS) before/after builds


-------------------------------------------------------------------------------------------------
                                               FAQ
-------------------------------------------------------------------------------------------------

- Do you support Unity Free? -
If you want to automatically build multiple version of your game with Advanced Builder, you will
absolutely need Unity Pro (because custom builds requires Unity Pro). BUT you can manually set a
configuration via the 'Project Configuration' window. So you'll have to click on a button to set
the configuration you want, and then build manually every configuration. At the end of each build,
you just select another configuration and build manually again etc...

- Can I use Advanced Builder with Unity batch mode? -
Unity batch mode allows you to launch Unity via a command line instruction and perform builds
without having Unity opened. It allows you to perform automated builds on a production computer
for example, building your app every x hours after performing a version control update etc...
This will be added really soon in a future update.

- Will you add support for more platforms? -
We currently support Android, iOS, WebPlayer, Windows, Mac, Linux, Windows Phone 8, Windows Store 8
and BlackBerry. If you need a platform that is not listed here, please contact us!

- Can I use Advanced Builder with Prime31 plugins? -
Yes, there is absolutely no issue using prime31 plugins, because Advanced Builder generate builds
using Unity native build system.

- Why my BlackBerry final file name has its spaces removed? -
It's currently a Unity bug, if you have any space in the name of the .bar file, the build will
just fail. So Advanced Builder remove them automatically.

- What's the minimum Unity version required? -
Advanced Builder will work starting with Unity 4.1.

- Can I build for iOS using a Windows/Linux computer? -
As Advanced Builder uses the Unity API for the build process, and Unity does not allow you to build
for iOS on a machine that is not a Mac, building for iOS absolutely require having a Mac.