using UnityEngine;
using UnityEditor;

// Modified from http://wiki.unity3d.com/index.php?title=CreateScriptableObjectAsset
namespace com.pygmymonkey.tools
{
	public static class Utils
	{
		public static void DrawCategoryLabel(string label)
		{
			GUILayout.Space(5f);
		
			GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1f)});
			EditorGUILayout.LabelField(label, Style.LabelCategoryStyle);
			GUILayout.Space(2f);
			GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1f)});
			
			GUILayout.Space(5f);
		}


		public static bool DrawBigButton(string label) { return DrawBigButton(label, Colors.Default); }
		public static bool DrawBigButton(string label, Color color) { return DrawBigButton(label, color, Style.ButtonStyle); }
		public static bool DrawBigButton(string label, Color color, GUIStyle style)
		{
			bool result = false;
			
			GUI.backgroundColor = color;
			
			if (GUILayout.Button(label, style))
			{
				result = true;
			}
			
			GUI.backgroundColor = Colors.Default;
			
			return result;
		}

		public static bool DrawDeleteRedButton() { return DrawDeleteRedButton("Delete"); }
		public static bool DrawDeleteRedButton(string name)
		{
			return DrawBigButton(name, Colors.Red, Style.ButtonDeleteStyle);
		}


		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title)
		{
			bool isActive = false;
			return DrawHeader(title, false, ref isActive);
		}

		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title, bool isLocked)
		{
			return DrawHeader(title, title, isLocked);
		}

		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title, string key)
		{
			bool isActive = false;
			return DrawHeader(title, key, false, ref isActive, false);
		}

		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title, string key, bool isLocked)
		{
			bool isActive = false;
			return DrawHeader(title, key, false, ref isActive, isLocked);
		}

		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title, bool drawToggle, ref bool isEnabled)
		{
			return DrawHeader(title, title, drawToggle, ref isEnabled, false);
		}

		/// <summary>
		/// Draw a distinctly different looking header label
		/// </summary>
		public static bool DrawHeader(string title, string key, bool drawToggle, ref bool isEnabled, bool isLocked)
		{
			bool state = isLocked ? true : EditorPrefs.GetBool(key, true);

			GUILayout.Space(5f);

			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(3f);
				
				bool hasStateChanged = false;

				if (drawToggle)
				{
					isEnabled = EditorGUILayout.Toggle(string.Empty, isEnabled, GUILayout.Width(15f), GUILayout.Height(15f));
				}

				if (!state)
				{
					GUI.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
				}

				if (!GUILayout.Toggle(true, "<b><size=11>" + title + "</size></b>", "dragtab"))
				{
					if (!isLocked)
					{
						state = !state;
						hasStateChanged = true;
					}
				}
				
				if (hasStateChanged)
				{
					if (!isLocked)
					{
						EditorPrefs.SetBool(key, state);
					}
				}
				
				GUILayout.Space(2f);
			}
			EditorGUILayout.EndHorizontal();
			
			GUI.backgroundColor = Colors.Default;
			
			if (!state)
			{
				GUILayout.Space(3f);
			}
			
			return state;
		}


		/// <summary>
		/// Begin drawing the content area.
		/// </summary>
		public static void BeginContents ()
		{
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(4f);
			EditorGUILayout.BeginHorizontal("AS TextArea", GUILayout.MinHeight(10f));
			EditorGUILayout.BeginVertical();
			GUILayout.Space(4f);
		}


		/// <summary>
		/// End drawing the content area.
		/// </summary>
		public static void EndContents ()
		{
			GUILayout.Space(5f);
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
			GUILayout.Space(3f);
			EditorGUILayout.EndHorizontal();
		}


		public static void RefreshAssets()
		{
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}
}