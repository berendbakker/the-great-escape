﻿using UnityEngine;

namespace com.pygmymonkey.tools
{
	public class Colors
	{
		public static readonly Color Default = Color.white;
		public static readonly Color Green = Color.green;
		public static readonly Color Red = Color.red;
	}
}