using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	public class AdvancedSettingsRenderer : IDefaultRenderer
	{
		/*
		 * AdvancedSettings data
		 */
		private AdvancedSettings m_advancedSettings;
		
		
		/*
		 * Constructor
		 */
		public AdvancedSettingsRenderer(AdvancedSettings advancedSettings)
		{
			m_advancedSettings = advancedSettings;
			
			if (string.IsNullOrEmpty(advancedSettings.customPath))
			{
				setDefaultCustomPath();
			}
		}
		
		
		/*
		 * Reset custom path
		 */
		private void setDefaultCustomPath()
		{
			m_advancedSettings.customPath = "$BUILD_DATE/$RELEASE_TYPE/$PLATFORM/$DISTRIB_PLATFORM/$ARCHITECTURE/";
		}
		
		
		/*
		 * Draw in inspector
		 */
		public void drawInspector()
		{
			/*
			 * BUILD FOLDER SETTINGS
			 */

			Utils.DrawCategoryLabel("Build folder path");
			
			/*
			 * Display settings for the final build folder
			 */

			
			EditorGUILayout.LabelField("$BUILD_DATE \t\t\t\t\t The build date (format: yy-MM-dd HHhmm)");
			EditorGUILayout.LabelField("$RELEASE_TYPE \t\t\t\t The release type");
			EditorGUILayout.LabelField("$PLATFORM \t\t\t\t\t The platform (example: iOS, Android, Mac...)");
			EditorGUILayout.LabelField("$DISTRIB_PLATFORM \t\t The distributionplatform (example: Google Play, Apple Store...)");
			EditorGUILayout.LabelField("$ARCHITECTURE \t\t\t\t The platform architecture (Universal, x86, x64...)");

			GUI.color = isCustomPathValid() ? Colors.Default : Colors.Red;
			m_advancedSettings.customPath = EditorGUILayout.TextField(m_advancedSettings.customPath);
			GUI.color = Colors.Default;

			if (GUILayout.Button("Reset custom path to default"))
			{
				setDefaultCustomPath();
				
				// We release focus on the text field so unity can update it
				GUIUtility.keyboardControl = 0;
				GUIUtility.hotControl = 0;
			}
			
			
			
			/*
			 * CUSTOM BUILD SETTINGS
			 */
			GUILayout.Space(20f);
			Utils.DrawCategoryLabel("Custom build");
			
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField("Custom build script");
				m_advancedSettings.customBuildMonoScript = (MonoScript)EditorGUILayout.ObjectField(m_advancedSettings.customBuildMonoScript == null ? null : m_advancedSettings.customBuildMonoScript, typeof(MonoScript), false);
			}
			EditorGUILayout.EndHorizontal();
			
			
			
			/*
			 * BUILD OPTIONS
			 */
			GUILayout.Space(20f);
			Utils.DrawCategoryLabel("Build options");

			drawSingleBuildOption("Open build folder after build", ref m_advancedSettings.openBuildFolder);
			drawSingleBuildOption("Development build", ref m_advancedSettings.isDevelopmentBuild);
			drawSingleBuildOption("Autoconnect Profiler", ref m_advancedSettings.shouldAutoconnectProfiler);
			drawSingleBuildOption("Autorun Build", ref m_advancedSettings.shouldAutoRunPlayer);
			drawSingleBuildOption("Allow script debugging", ref m_advancedSettings.allowDebugging);
			drawSingleBuildOption("Append project (iOS)", ref m_advancedSettings.appendProject);
			drawSingleBuildOption("Use Symlink Libraries (iOS)", ref m_advancedSettings.useSymlinkLibraries);
			drawSingleBuildOption("Check to show error if android keystore passwords are not set (Android)", ref m_advancedSettings.checkAndroidKeystorePasswords);
		}


		private void drawSingleBuildOption(string name, ref bool isEnabled)
		{
			EditorGUILayout.BeginHorizontal();
			isEnabled = EditorGUILayout.Toggle(isEnabled, GUILayout.Width(15f));
			EditorGUILayout.LabelField(name);
			EditorGUILayout.EndHorizontal();
		}
		
		
		/*
		 * Check for warnings and errors
		 */
		public void checkWarningsAndErrors(ErrorReporter errorReporter)
		{
			if (!isCustomPathValid())
			{
				errorReporter.addError("Custom Path in AdvancedSettings has bad format");
			}

			if (!isCustomBuildMonoScriptValid())
			{
				errorReporter.addError("Your custom build script must implements the interface IAdvancedCustomBuild");
			}
		}
		
		
		private bool isCustomPathValid()
		{
			string path = m_advancedSettings.customPath
				.Replace("$BUILD_DATE", string.Empty)
				.Replace("$RELEASE_TYPE", string.Empty)
				.Replace("$PLATFORM", string.Empty)
				.Replace("$DISTRIB_PLATFORM", string.Empty)
				.Replace("$ARCHITECTURE", string.Empty);
			
			if (string.IsNullOrEmpty(path))
			{
				return false;
			}
			
			if (path.Contains("$"))
			{
				return false;
			}
			
			return true;
		}

		public bool isCustomBuildMonoScriptValid()
		{
			if (m_advancedSettings.customBuildMonoScript == null)
			{
				return true;
			}
			else if (m_advancedSettings.customBuildMonoScript.GetClass().GetInterface("IAdvancedCustomBuild") != null)
			{
				return true;
			}

			return false;
		}
	}
}