﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class AdvancedSettings
	{
		/*
		 * Custom path for the build
		 */
		public string customPath = "$BUILD_DATE/$RELEASE_TYPE/$PLATFORM/$DISTRIB_PLATFORM/$ARCHITECTURE/";
		
		
		/*
		 * Custom build script
		 */
		public MonoScript customBuildMonoScript;
		
		
		/*
		 * Open build folder at the end of every build
		 */
		public bool openBuildFolder = true;

		
		/*
		 * Is development build
		 */
		public bool isDevelopmentBuild = false;
		
		
		/*
		 * Should autoconnect to profiler
		 */
		public bool shouldAutoconnectProfiler = false;
		
		
		/*
		 * Should autorun player
		 */
		public bool shouldAutoRunPlayer = false;
		
		
		/*
		 * Allow debugging
		 */
		public bool allowDebugging = false;
		
		
		/*
		 * Append (rather than replace) the build of an iOS Xcode project
		 */
		public bool appendProject = false;
		
		
		/*
		 * Use symslink libraries
		 */
		public bool useSymlinkLibraries = false;
		
		
		/*
		 * Check to show error if android keystore passwords are not set
		 */
		public bool checkAndroidKeystorePasswords = false;
	}
}