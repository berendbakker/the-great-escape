using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	public class ProjectConfigurationRenderer : IDefaultRenderer
	{
		/*
		 * ProjectConfiguration data
		 */
		private AdvancedBuilder m_advancedBuilder;
		
		
		/*
		 * Constructor
		 */
		public ProjectConfigurationRenderer(AdvancedBuilder advancedBuilder)
		{
			m_advancedBuilder = advancedBuilder;
		}
		
		
		/*
		 * Draw in inspector
		 */
		public void drawInspector()
		{
			List<ReleaseType> activeReleaseTypeList = m_advancedBuilder.getReleaseTypes().getReleaseTypeList().Where(x => x.isActive).ToList();
			List<IPlatform> activePlatformList = m_advancedBuilder.getPlatforms().platformDictionary.Values.Where(x => x.getPlatformProperties().isActive).ToList();

			if (activeReleaseTypeList.Count == 0)
			{
				EditorGUILayout.HelpBox("You need to select at least one release type in the 'Release Type' window", MessageType.Warning, true);
				return;
			}

			if (activePlatformList.Count == 0)
			{
				EditorGUILayout.HelpBox("You need to select at least one platform in the 'Platforms' window", MessageType.Warning, true);
				return;
			}

			EditorGUILayout.HelpBox("Here you can apply a configuration to your current projet, so you can test your game inside the Editor in the configuration you want. Of course if you decide to build, every build will have the configuration you specified (see the Perform Build section below to see in detail every build that will be performed).", MessageType.Info, true);

			float[] widthArray = new float[] { 45.0f, 80.0f, 80.0f, 80.0f, 65.0f };

			GUILayout.BeginHorizontal();
			{
				EditorGUILayout.Space();
				EditorGUILayout.LabelField("Release Type", GUILayout.Width(widthArray[0]));
				EditorGUILayout.LabelField("Platform", GUILayout.Width(widthArray[1]));
				EditorGUILayout.LabelField("Distribution", GUILayout.Width(widthArray[3]));
				EditorGUILayout.LabelField("Architecture", GUILayout.Width(widthArray[2]));
				EditorGUILayout.LabelField("Tex. Com.", GUILayout.Width(widthArray[4]));
			}
			GUILayout.EndHorizontal();
			
			EditorGUILayout.Space();

			if (!isAtLeastOneConfigIsCurrent())
			{
				GUI.color = Color.yellow;
			}

			GUILayout.BeginHorizontal();
			{
				EditorGUILayout.Space();
				EditorGUILayout.LabelField("NONE", GUILayout.Width(widthArray[0]));
				EditorGUILayout.LabelField("NONE", GUILayout.Width(widthArray[1]));
				EditorGUILayout.LabelField("NONE", GUILayout.Width(widthArray[3]));
				EditorGUILayout.LabelField("NONE", GUILayout.Width(widthArray[2]));
				EditorGUILayout.LabelField("NONE", GUILayout.Width(widthArray[4]));
			}
			GUILayout.EndHorizontal();

			GUI.color = Color.white;

			foreach (ReleaseType releaseType in activeReleaseTypeList)
			{
				foreach (IPlatform platform in activePlatformList)
				{
					PlatformProperties platformProperties = platform.getPlatformProperties();

					foreach (DistributionPlatform distributionPlatform in platformProperties.getActiveDistributionPlatformList())
					{
						foreach (PlatformArchitecture platformArchitecture in platformProperties.getActivePlatformArchitectureList())
						{
							foreach (ITextureCompression textureCompression in platformProperties.getActiveTextureCompressionList())
							{
								bool isCurrentConfig = isCurrentConfiguration(releaseType, platformProperties, distributionPlatform, platformArchitecture, textureCompression);
								GUI.color = isCurrentConfig ? Color.green : Color.white;
								GUI.enabled = !isCurrentConfig;

								GUILayout.BeginHorizontal();
								{
									if (GUILayout.Button("Set"))
									{
										AppParameters.Get.updateParameters(releaseType.getFormattedName(), platformProperties.getFormattedPlatformType(), distributionPlatform.getFormattedName(), platformArchitecture.getFormattedName(), textureCompression.getName(), releaseType.productName, releaseType.bundleIdentifier, m_advancedBuilder.getProductParameters().bundleVersion);
										
										//TODO: This calls are already in PlatformBuilder, find a way to regroup them
										if (!UnityEditorInternal.InternalEditorUtility.HasPro())
										{
											PlayerSettings.bundleIdentifier = releaseType.bundleIdentifier;
											PlayerSettings.bundleVersion = m_advancedBuilder.getProductParameters().bundleVersion;
											PlayerSettings.productName = releaseType.productName;
											
											platform.setupAdditionalParameters(m_advancedBuilder.getProductParameters(), releaseType, platformArchitecture, textureCompression);
										}
									}

									EditorGUILayout.LabelField(releaseType.getFormattedName(), GUILayout.Width(widthArray[0]));
									EditorGUILayout.LabelField(platform.getPlatformProperties().getFormattedPlatformType(), GUILayout.Width(widthArray[1]));
									EditorGUILayout.LabelField(distributionPlatform.getFormattedName(), GUILayout.Width(widthArray[3]));
									EditorGUILayout.LabelField(platformArchitecture.getFormattedName() ?? "-", GUILayout.Width(widthArray[2]));
									EditorGUILayout.LabelField(textureCompression.getName(), GUILayout.Width(widthArray[4]));
								}
								GUILayout.EndHorizontal();

								GUI.color = Color.white;
								GUI.enabled = true;
							}
						}
					}
				}
			}
		}

		private bool isCurrentConfiguration(ReleaseType releaseType, PlatformProperties platformProperties, DistributionPlatform distributionPlatform, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			if (!checkParameters(AppParameters.Get.releaseType, releaseType.getFormattedName())) return false;
			if (!checkParameters(AppParameters.Get.platformType, platformProperties.getFormattedPlatformType())) return false;
			if (!checkParameters(AppParameters.Get.distributionPlatform, distributionPlatform.getFormattedName())) return false;
			if (!checkParameters(AppParameters.Get.platformArchitecture, platformArchitecture.getFormattedName())) return false;
			if (!checkParameters(AppParameters.Get.textureCompression, textureCompression.getName())) return false;

			return true;
		}

		private bool checkParameters(string param1, string param2)
		{
			if (!(string.IsNullOrEmpty(param1) && string.IsNullOrEmpty(param2)) && !param1.Equals(param2))
			{
				return false;
			}

			return true;
		}

		private bool isAtLeastOneConfigIsCurrent()
		{
			List<ReleaseType> activeReleaseTypeList = m_advancedBuilder.getReleaseTypes().getReleaseTypeList().Where(x => x.isActive).ToList();
			List<IPlatform> activePlatformList = m_advancedBuilder.getPlatforms().platformDictionary.Values.Where(x => x.getPlatformProperties().isActive).ToList();
			
			foreach (ReleaseType releaseType in activeReleaseTypeList)
			{
				foreach (IPlatform platform in activePlatformList)
				{
					PlatformProperties platformProperties = platform.getPlatformProperties();
					
					foreach (DistributionPlatform distributionPlatform in platformProperties.getActiveDistributionPlatformList())
					{
						foreach (PlatformArchitecture platformArchitecture in platformProperties.getActivePlatformArchitectureList())
						{
							foreach (ITextureCompression textureCompression in platformProperties.getActiveTextureCompressionList())
							{
								if (isCurrentConfiguration(releaseType, platformProperties, distributionPlatform, platformArchitecture, textureCompression))
								{
									return true;
								}
							}
						}
					}
				}
			}

			return false;
		}
		
		/*
		 * Check for warnings and errors
		 */
		public void checkWarningsAndErrors(ErrorReporter errorReporter)
		{
			if (!isAtLeastOneConfigIsCurrent())
			{
				if (!string.IsNullOrEmpty(AppParameters.Get.releaseType))
				{
					AppParameters.Get.updateParameters(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
				}

				errorReporter.addWarning("If you want to try a specific configuration directly in the Unity Editor, you need to select at least one configuration in the 'Project Configuration' window above.");
			}
		}
	}
}