using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.pygmymonkey.tools
{
	public class PlatformsRenderer : IDefaultRenderer
	{
		/*
		 * Platforms data
		 */
		private Platforms m_platforms;
		
		
		/*
		 * Constructor
		 */
		public PlatformsRenderer(Platforms platforms)
		{
			m_platforms = platforms;
			
			m_platformRendererList = new List<IPlatformRenderer>()
			{
				new PlatformRenderer(m_platforms.getPlatformAndroid(), new PlatformAndroidAdditionalRenderer(m_platforms.getPlatformAndroid()), updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformiOS(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformWebPlayer(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformWindows(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformMac(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformLinux(), null, updateSupportedPlatformList),
				#if !UNITY_4_0 && !UNITY_4_1
				new PlatformRenderer(m_platforms.getPlatformWindowsPhone8(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformWindowsStore8(), null, updateSupportedPlatformList),
				new PlatformRenderer(m_platforms.getPlatformBlackBerry(), null, updateSupportedPlatformList),
				#endif
			};
			
			updateSupportedPlatformList();
		}
		
		
		/*
		 * Some private parameters
		 */
		private static int m_platformListIndex;
		private List<IPlatform> m_platformSupportedList = new List<IPlatform>();
		private List<IPlatformRenderer> m_platformRendererList;
		
		
		/*
		 * Get platform renderer from platform type
		 */
		private IPlatformRenderer getPlatformRendererFromPlatform(PlatformType platformType)
		{
			switch(platformType)
			{
			case PlatformType.Android:
				return m_platformRendererList[0];
				
			case PlatformType.iOS:
				return m_platformRendererList[1];
				
			case PlatformType.WebPlayer:
				return m_platformRendererList[2];
				
			case PlatformType.Windows:
				return m_platformRendererList[3];
				
			case PlatformType.Mac:
				return m_platformRendererList[4];
				
			case PlatformType.Linux:
				return m_platformRendererList[5];
			
			#if !UNITY_4_0 && !UNITY_4_1
			case PlatformType.WindowsPhone8:
				return m_platformRendererList[6];

			case PlatformType.WindowsStore8:
				return m_platformRendererList[7];

			case PlatformType.BlackBerry:
				return m_platformRendererList[8];
			#endif

			default:
				throw new Exception("The platform " + platformType + " does not have a platform renderer");
			}
		}


		private IEnumerable<KeyValuePair<IPlatform, IPlatformRenderer>> getSupportedPlatformRenderers()
		{
			IEnumerable<IPlatform> platforms = m_platforms.platformDictionary.Values;
			IEnumerable<IPlatform> supportedPlatforms = platforms.Where(x => x.getPlatformProperties().isSupported());
			IEnumerable<KeyValuePair<IPlatform, IPlatformRenderer>> platformsPair = supportedPlatforms.Select(x => new KeyValuePair<IPlatform, IPlatformRenderer>(x, getPlatformRendererFromPlatform(x.getPlatformProperties().platformType)));

			return platformsPair;
		}


		public List<IPlatformRenderer> getActivePlatformRenderers()
		{
			IEnumerable<IPlatform> platforms = m_platforms.platformDictionary.Values;
			IEnumerable<IPlatform> activePlatforms = platforms.Where(x => x.getPlatformProperties().isActive);

			return activePlatforms.Select(x => getPlatformRendererFromPlatform(x.getPlatformProperties().platformType)).ToList();
		}


		/*
		 * Draw in inspector
		 */
		public void drawInspector()
		{
			IEnumerable<KeyValuePair<IPlatform, IPlatformRenderer>> supportedPlatformList = getSupportedPlatformRenderers();

			foreach (KeyValuePair<IPlatform, IPlatformRenderer> supportedPlatform in supportedPlatformList)
			{
				IPlatform platform = supportedPlatform.Key;
				IPlatformRenderer platformRenderer = supportedPlatform.Value;

				if (Utils.DrawHeader(platform.getPlatformProperties().name, true, ref platform.getPlatformProperties().isActive))
				{
					Utils.BeginContents();
					platformRenderer.drawInspector();
					Utils.EndContents();
				}
			}

			if (supportedPlatformList.Count() > 0)
			{
				GUILayout.Space(10f);
			}
			
			drawSupportedPlatformList();
		}

		
		/*
		 * Check for warnings and errors
		 */
		public void checkWarningsAndErrors(ErrorReporter errorReporter)
		{
			IEnumerable<KeyValuePair<IPlatform, IPlatformRenderer>> supportedPlatformList = getSupportedPlatformRenderers();

			foreach (KeyValuePair<IPlatform, IPlatformRenderer> supportedPlatform in supportedPlatformList)
			{
				IPlatformRenderer platformRenderer = supportedPlatform.Value;
				platformRenderer.checkWarningsAndErrors(errorReporter);
			}

			/*
			 * If no platform is added
			 */
			if (m_platforms.getPlatformActiveCount() == 0)
			{
				errorReporter.addError("You need to have at least one platform active");
			}
			else
			{
				/*
				 * If no distributionplatform is activated at all
				 */
				int totalDistributionPlatformUsedCount = m_platforms.platformDictionary.Values.Where(x => x.getPlatformProperties().isActive).Sum(x => x.getPlatformProperties().getActiveDistributionPlatformList().Count);

				if (totalDistributionPlatformUsedCount == 0)
				{
					errorReporter.addError("No build will be performed\nYou must have at least one distribution platform selected, see warnings");
				}


				/*
				 * If no platform architecture is activated at all
				 */
				int totalPlatformArchitectureUsedCount = m_platforms.platformDictionary.Values.Where(x => x.getPlatformProperties().isActive).Sum(x => x.getPlatformProperties().getActivePlatformArchitectureList().Count);
				
				if (totalPlatformArchitectureUsedCount == 0)
				{
					errorReporter.addError("No build will be performed\nYou must have at least one platform architecture selected, see warnings");
				}


				/*
				 * If no textureCompression is activated at all
				 */
				int totalTextureCompressionUsedCount = m_platforms.platformDictionary.Values.Where(x => x.getPlatformProperties().isActive).Sum(x => x.getPlatformProperties().getActiveTextureCompressionList().Count);

				if (totalTextureCompressionUsedCount == 0)
				{
					errorReporter.addError("No build will be performed\nYou must have at least one texture compression selected, see warnings");
				}
			}
		}
		
		
		/*
		 * Draw the inspector popup with available platforms
		 */
		private void drawSupportedPlatformList()
		{
			bool isAtLeastOnePlatformSupported = (m_platforms.platformDictionary.Values.Where(x => !x.getPlatformProperties().isSupported()).Count() != 0);
			if (isAtLeastOnePlatformSupported)
			{
				EditorGUILayout.BeginVertical(Style.CenterMarginStyle);
				{
					String[] platformSupportedStringArray = m_platformSupportedList.Select(p => p.getPlatformProperties().name).ToArray();

					m_platformListIndex = EditorGUILayout.Popup(m_platformListIndex, platformSupportedStringArray);
					if (GUILayout.Button("Add Platform"))
					{
						PlatformType paltformType = m_platformSupportedList[m_platformListIndex].getPlatformProperties().platformType;
						m_platforms.platformDictionary[paltformType].getPlatformProperties().setSupported(true);
						updateSupportedPlatformList();
					}
				}
				EditorGUILayout.EndVertical();
			}
		}
		
		
		/*
		 * Update the available platform list
		 */
		private void updateSupportedPlatformList()
		{
			m_platformSupportedList = m_platforms.platformDictionary.Values.Where(x => !x.getPlatformProperties().isSupported()).ToList();

			if (m_platformListIndex >= m_platformSupportedList.Count)
			{
				m_platformListIndex = m_platformSupportedList.Count - 1;
			}
			
			if (m_platformListIndex < 0)
			{
				m_platformListIndex = 0;
			}
		}
	}
}