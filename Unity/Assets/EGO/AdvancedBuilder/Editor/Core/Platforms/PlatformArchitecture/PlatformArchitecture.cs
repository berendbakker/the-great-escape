﻿using UnityEditor;
using System;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformArchitecture
	{
		/*
		 * Determine if the architecture will be used
		 */
		public bool isActive;
		
		
		/*
		 * The architecture name
		 */
		public readonly string name;


		/*
		 * The build target
		 */
		public readonly BuildTarget buildTarget;


		/*
		 * The binary suffix
		 */
		public readonly string binarySuffix;

		
		public PlatformArchitecture(string name, string binarySuffix, BuildTarget buildTarget, bool isActive = false)
		{
			this.name = name;
			this.binarySuffix = binarySuffix;
			this.buildTarget = buildTarget;
			this.isActive = isActive;
		}

		public string getFormattedName()
		{
			return name;
		}
	}
}