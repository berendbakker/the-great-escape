namespace com.pygmymonkey.tools
{
	public class DefaultTextureCompression : ITextureCompression
	{
		private bool m_isActive = true;

		public bool isActive
		{
			get { return m_isActive; }
			set { m_isActive = value; }
		}
		
	    public string getName()
		{
	        return "Default";
	    }
	}
}