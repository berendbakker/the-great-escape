﻿using UnityEditor;
using UnityEngine;
using System;

namespace com.pygmymonkey.tools
{
	public class PlatformAndroidAdditionalRenderer : IPlatformAdditionalRenderer
	{
		private PlatformAndroid m_platformAndroid;

		public PlatformAndroidAdditionalRenderer(PlatformAndroid platformAndroid)
		{
			m_platformAndroid = platformAndroid;
		}

		/*
		 * Draw the summary of things that are going to be build for this platform
		 */
		public void drawAdditionalBuildSummary(ITextureCompression textureCompression, string bundleVersion)
		{
			TextureCompressionAndroid textureCompressionAndroid = (TextureCompressionAndroid)textureCompression;

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Bundle Version Code", GUILayout.Width(143f));
			EditorGUILayout.LabelField(m_platformAndroid.getAndroidBundleVersionCode(textureCompressionAndroid, bundleVersion).ToString());
			EditorGUILayout.EndHorizontal();
		}
	}
}