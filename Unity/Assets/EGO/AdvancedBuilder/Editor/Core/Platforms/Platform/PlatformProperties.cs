﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformProperties
	{
		/*
		 * Define if this platform is used
		 */
		public bool isActive;


		/*
		 * Determine if this platform is added to the list of supported platforms
		 */
		[SerializeField]
		private bool m_isSupported;
		
		public void setSupported(bool isSupported)
		{
			m_isSupported = isSupported;
			isActive = isSupported;
		}
		
		public bool isSupported()
		{
			return m_isSupported;
		}


		/*
		 * List of distributionplatform for the platform
		 */
		[SerializeField]
		private List<DistributionPlatform> m_distributionPlatformList = new List<DistributionPlatform>();

		public List<DistributionPlatform> getDistributionPlatformList()
		{
			return m_distributionPlatformList;
		}

		public List<DistributionPlatform> getActiveDistributionPlatformList()
		{
			List<DistributionPlatform> distributionPlatformList = m_distributionPlatformList.ToList();
			if (distributionPlatformList.Count == 0)
			{
				distributionPlatformList.Add(new DistributionPlatform("Default", true));
			}

			return distributionPlatformList.Where(x => x.isActive).ToList();
		}
		
		
		/*
		 * List of platform architecture
		 */
		[SerializeField]
		private List<PlatformArchitecture> m_platformArchitectureList = new List<PlatformArchitecture>();
		
		public List<PlatformArchitecture> getPlatformArchitectureList()
		{
			return m_platformArchitectureList;
		}
		
		public List<PlatformArchitecture> getActivePlatformArchitectureList()
		{
			return m_platformArchitectureList.Where(x => x.isActive).ToList();
		}

		public bool isUsingPlatformArchitecture()
		{
			return m_platformArchitectureList.Count(x => !string.IsNullOrEmpty(x.name)) != 0;
		}
		
		
		/*
		 * List of texture compressions
		 */
		private List<ITextureCompression> m_textureCompressionList;

		public List<ITextureCompression> getTextureCompressionList()
		{
			return m_textureCompressionList;
		}

		public List<ITextureCompression> getActiveTextureCompressionList()
		{
			return m_textureCompressionList.Where(x => x.isActive).ToList();
		}

		public bool isUsingTextureCompression()
		{
			return m_textureCompressionList.Count(x => x.GetType() != typeof(DefaultTextureCompression)) != 0;
		}


		/*
		 * Platform type
		 */
		public readonly PlatformType platformType;
		
		
		/*
		 * Platform name
		 */
		public readonly string name;


		public string getFormattedPlatformType()
		{
			return platformType.ToString();
		}

		/*
		 * Constructor
		 */
		public PlatformProperties(PlatformType platformType, string name, List<DistributionPlatform> distributionPlatformList, List<PlatformArchitecture> platformArchitectureList, List<ITextureCompression> textureCompressionList)
		{
			this.platformType = platformType;
			this.name = name;
			m_distributionPlatformList = distributionPlatformList;
			m_platformArchitectureList = platformArchitectureList;
			m_textureCompressionList = textureCompressionList;
		}

		
		
		/*
		 * Return the build destination
		 */
		public string getBuildDestinationPath(AdvancedSettings advancedSettings, ReleaseType releaseType, DistributionPlatform distributionPlatform, PlatformArchitecture platformArchitecture, DateTime buildDate)
		{
			string destinationName = advancedSettings.customPath;
			
			destinationName = destinationName.Replace("$BUILD_DATE", buildDate.ToString("yy-MM-dd HH\\hmm"));
			destinationName = destinationName.Replace("$RELEASE_TYPE", releaseType.name);
			destinationName = destinationName.Replace("$PLATFORM", name);

			if (!distributionPlatform.name.Equals("Default"))
			{
				destinationName = destinationName.Replace("$DISTRIB_PLATFORM", distributionPlatform.name);
			}
			else
			{
				destinationName = destinationName.Replace("$DISTRIB_PLATFORM/", string.Empty).Replace("$DISTRIB_PLATFORM", string.Empty);
			}

			if (!string.IsNullOrEmpty(platformArchitecture.name))
			{
				destinationName = destinationName.Replace("$ARCHITECTURE", platformArchitecture.name);
			}
			else
			{
				destinationName = destinationName.Replace("$ARCHITECTURE/", string.Empty).Replace("$ARCHITECTURE", string.Empty);
			}

			return destinationName;
		}
		
		
		/*
		 * Return the build file name
		 */
		public string getBuildFileName(ReleaseType releaseType, IPlatform platform, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			string productName = releaseType.productName;
			
			if (isUsingTextureCompression())
			{
				productName += " - " + textureCompression.getName();
			}

			productName += platformArchitecture.binarySuffix;
			productName = platform.formatFinalFileName(productName);

			return productName;
		}
		
		
		/*
		 * Return the number of build we're going to make for that platform
		 */
		public int getBuildCount()
		{
			if (!isActive)
			{
				return 0;
			}
			
			return getActiveDistributionPlatformList().Count * getActivePlatformArchitectureList().Count * getActiveTextureCompressionList().Count;
		}
		
		
		/*
		 * Return the final build path
		 */
		public string getBuildPath(AdvancedSettings settings, ReleaseType releaseType, IPlatform platform, DistributionPlatform distributionPlatformList, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression, DateTime buildDate)
		{
			return "/Builds/" + getBuildDestinationPath(settings, releaseType, distributionPlatformList, platformArchitecture, buildDate) + "" + getBuildFileName(releaseType, platform, platformArchitecture, textureCompression);
		}
	}
}
