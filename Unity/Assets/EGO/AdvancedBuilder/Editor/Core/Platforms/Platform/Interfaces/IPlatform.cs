namespace com.pygmymonkey.tools
{
	public interface IPlatform
	{
		/*
		 * Platform common properties
		 */
		PlatformProperties getPlatformProperties();


		/*
		 * Set up additional parameters
		 */
		void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression);


		/*
		 * Format final file name
		 */
		string formatFinalFileName(string fileName);
		
		
		/*
		 * Return specific platform errors
		 */
		string getPlatformError();
	}
}