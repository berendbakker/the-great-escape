using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformMac : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}
		
		
		/*
		 * Constructor
		 */
		public PlatformMac()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.Mac,
				"Mac",
				new List<DistributionPlatform> { },
				new List<PlatformArchitecture>
				{
					new PlatformArchitecture("OSX x86", ".app", BuildTarget.StandaloneOSXIntel, true),
					#if !UNITY_4_0 && !UNITY_4_1
					new PlatformArchitecture("OSX x86_64", ".app", BuildTarget.StandaloneOSXIntel64),
					new PlatformArchitecture("OSX Universal", ".app", BuildTarget.StandaloneOSXUniversal),
					#endif
				},
				new List<ITextureCompression>() { new DefaultTextureCompression() }
			);
		}
		
		
		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
		}
		
		
		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			return fileName;
		}
		
		
		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			return null;
		}
	}
}