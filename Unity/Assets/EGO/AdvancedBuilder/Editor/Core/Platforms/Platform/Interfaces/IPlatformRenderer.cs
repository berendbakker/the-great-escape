namespace com.pygmymonkey.tools
{
	public interface IPlatformRenderer : IDefaultRenderer
	{
		/*
		 * Draw the summary of things that are going to be build for this platform
		 */
		void drawBuildSummary(AdvancedSettings settings, ReleaseType releaseType, string bundleVersion);
	}
}