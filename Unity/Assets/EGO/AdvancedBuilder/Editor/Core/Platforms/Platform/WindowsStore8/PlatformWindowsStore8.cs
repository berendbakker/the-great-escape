#if !UNITY_4_0 && !UNITY_4_1
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformWindowsStore8 : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}
		
		
		/*
		 * Windows Store 8 Build types
		 */
		private string m_visualStudioCppDX = "D3D11 C++ Solution";
		private string m_visualStudioCSharpDX = "D3D11 C# Solution";
		private string m_visualStudioCpp = "XAML C++ Solution";
		private string m_visualStudioCSharp = "XAML C# Solution";
		
		
		/*
		 * Constructor
		 */
		public PlatformWindowsStore8()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.WindowsStore8,
				"Windows Store 8",
				new List<DistributionPlatform> { },
				new List<PlatformArchitecture>
				{
					new PlatformArchitecture(m_visualStudioCppDX, "/", BuildTarget.MetroPlayer),
					new PlatformArchitecture(m_visualStudioCSharpDX, "/", BuildTarget.MetroPlayer, true),
					new PlatformArchitecture(m_visualStudioCpp, "/", BuildTarget.MetroPlayer),
					new PlatformArchitecture(m_visualStudioCSharp, "/", BuildTarget.MetroPlayer),
				},
				new List<ITextureCompression>() { new DefaultTextureCompression() }
			);
		}
		
		
		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			if (platformArchitecture.name.Equals(m_visualStudioCppDX))
			{
				EditorUserBuildSettings.metroBuildType = MetroBuildType.VisualStudioCppDX;
			}
			else if (platformArchitecture.name.Equals(m_visualStudioCSharpDX))
			{
				EditorUserBuildSettings.metroBuildType = MetroBuildType.VisualStudioCSharpDX;
			}
			else if (platformArchitecture.name.Equals(m_visualStudioCpp))
			{
				EditorUserBuildSettings.metroBuildType = MetroBuildType.VisualStudioCpp;
			}
			else if (platformArchitecture.name.Equals(m_visualStudioCSharp))
			{
				EditorUserBuildSettings.metroBuildType = MetroBuildType.VisualStudioCSharp;
			}
		}
		
		
		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			return fileName;
		}
		
		
		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			return null;
		}
	}
}
#endif