using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformAndroid : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}
		
		
		/*
		 * List of supported texture compression
		 */
		[SerializeField]
		private List<TextureCompressionAndroid> m_textureCompressionList = new List<TextureCompressionAndroid>()
		{
			new TextureCompressionAndroid(AndroidBuildSubtarget.PVRTC,		14),
			new TextureCompressionAndroid(AndroidBuildSubtarget.DXT,		13),
			new TextureCompressionAndroid(AndroidBuildSubtarget.ATC,		12),
			new TextureCompressionAndroid(AndroidBuildSubtarget.ETC,		11),
			new TextureCompressionAndroid(AndroidBuildSubtarget.Generic,	10, true),
		};
		
		
		/*
		 * Constructor
		 */
		public PlatformAndroid()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.Android,
				"Android",
				new List<DistributionPlatform>
				{
					new DistributionPlatform("Google Play", true),
					new DistributionPlatform("Amazon Store"),
					new DistributionPlatform("Samsung AppStore")
				},
				new List<PlatformArchitecture>
				{
					new PlatformArchitecture(null, ".apk", BuildTarget.Android, true),
				},
				m_textureCompressionList.Cast<ITextureCompression>().ToList()
			);
		}
		
		
		/*
		 * Return Android BundleVersion based on TextureCompression and BundleVersion
		 * Format: xxyyy
		 * xx: Texture compression number
		 * yyy: BundleVersion number (0.3.0 -> 030)
		 */
		public int getAndroidBundleVersionCode(TextureCompressionAndroid textureCompressionAndroid, string bundleVersion)
		{
			return int.Parse(textureCompressionAndroid.versionCodePrefix + Regex.Replace(bundleVersion, "[^0-9]", ""));
		}


		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			TextureCompressionAndroid textureCompressionAndroid = (TextureCompressionAndroid)textureCompression;

			PlayerSettings.Android.bundleVersionCode = getAndroidBundleVersionCode(textureCompressionAndroid, productParameters.bundleVersion);
			EditorUserBuildSettings.androidBuildSubtarget = textureCompressionAndroid.subTarget;
		}
		
		
		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			return fileName;
		}


		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			AdvancedBuilder advancedBuilder = (AdvancedBuilder)AssetDatabase.LoadAssetAtPath("Assets/AdvancedBuilder/Editor/AdvancedBuilder.asset", typeof(AdvancedBuilder));

			if (advancedBuilder.getAdvancedSettings().checkAndroidKeystorePasswords)
			{
				if (PlayerSettings.keystorePass.Length == 0)
				{
					return "You need to define your Android Keystore password in Edit -> Project Settings -> Player -> Android -> Publishing Settings.";
				}
				else if (PlayerSettings.keyaliasPass.Length == 0)
				{
					return "You need to define your Android Alias password in Edit -> Project Settings -> Player -> Android -> Publishing Settings.";
				}
			}

			return null;
		}
	}
}