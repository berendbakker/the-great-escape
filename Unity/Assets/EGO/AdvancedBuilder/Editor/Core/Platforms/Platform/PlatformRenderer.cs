using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.pygmymonkey.tools
{
	public class PlatformRenderer : IPlatformRenderer
	{
		/*
		 * PlatformDefault data
		 */
		private IPlatform m_platform;
		private IPlatformAdditionalRenderer m_platformAdditionalRenderer;
		private Action m_updateAvailablePlatformListAction;
		
		
		/*
		 * Constructor
		 */
		public PlatformRenderer(IPlatform platform, IPlatformAdditionalRenderer platformAdditionalRenderer, Action updateAvailablePlatformList)
		{
			m_platform = platform;
			m_platformAdditionalRenderer = platformAdditionalRenderer;
			m_updateAvailablePlatformListAction = updateAvailablePlatformList;
		}
		
		
		/*
		 * Draw in inspector
		 */
		public void drawInspector()
		{
			PlatformProperties platformProperties = m_platform.getPlatformProperties();

			#if !UNITY_4_0 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_EDITOR_OSX
			if (m_platform.getPlatformProperties().platformType == PlatformType.iOS)
			{
				platformProperties.isActive = false;
				EditorGUILayout.HelpBox("You can only build for iOS if you're on a Mac", MessageType.Warning, true);
			}
			#endif

			DistributionsPlatformRenderer distributionsPlatformRenderer = new DistributionsPlatformRenderer(platformProperties.getDistributionPlatformList());
			distributionsPlatformRenderer.drawInspector();

			GUILayout.Space(20f);

			if (platformProperties.isUsingPlatformArchitecture())
			{
				Utils.DrawCategoryLabel("Architectures");
				foreach (PlatformArchitecture platformArchitecture in platformProperties.getPlatformArchitectureList())
				{
					EditorGUILayout.BeginHorizontal();
					platformArchitecture.isActive = EditorGUILayout.Toggle(platformArchitecture.isActive, GUILayout.Width(15f));
					EditorGUILayout.LabelField(platformArchitecture.name);
					EditorGUILayout.EndHorizontal();
				}
				
				GUILayout.Space(20f);
			}

			if (platformProperties.isUsingTextureCompression())
			{
				Utils.DrawCategoryLabel("Textures Compression");
				foreach (ITextureCompression textureCompression in platformProperties.getTextureCompressionList())
				{
					EditorGUILayout.BeginHorizontal();
					textureCompression.isActive = EditorGUILayout.Toggle(textureCompression.isActive, GUILayout.Width(15f));
					EditorGUILayout.LabelField(textureCompression.getName());
					EditorGUILayout.EndHorizontal();
				}
			}
			
			
			GUILayout.Space(10f);
			
				
			if (Utils.DrawDeleteRedButton("Delete platform"))
			{
				platformProperties.setSupported(false);
				m_updateAvailablePlatformListAction();
			}
		}
		
		
		/*
		 * Check for warnings and errors
		 */
		public void checkWarningsAndErrors(ErrorReporter errorReporter)
		{
			PlatformProperties platformProperties = m_platform.getPlatformProperties();
			
			if (!platformProperties.isActive)
			{
				return;
			}

			if (!string.IsNullOrEmpty(m_platform.getPlatformError()))
			{
				errorReporter.addError(m_platform.getPlatformError());
			}

			DistributionsPlatformRenderer distributionsPlatformRenderer = new DistributionsPlatformRenderer(platformProperties.getDistributionPlatformList());
			distributionsPlatformRenderer.checkWarningsAndErrors(errorReporter);

			if (platformProperties.getActivePlatformArchitectureList().Count == 0)
			{
				errorReporter.addWarning("No build will be performed for the platform '" + platformProperties.name + "'\nYou must select at least one platform architecture for this platform");
			}
			else if (platformProperties.getActiveTextureCompressionList().Count == 0)
			{
				errorReporter.addWarning("No build will be performed for the platform '" + platformProperties.name + "'\nYou must select at least one texture compression for this platform");
			}
		}
		
		
		/*
		 * Draw the summary of things that are going to be build for this platform
		 */
		public void drawBuildSummary(AdvancedSettings advancedSettings, ReleaseType releaseType, string bundleVersion)
		{
			PlatformProperties platformProperties = m_platform.getPlatformProperties();
			
			if (!platformProperties.isSupported())
			{
				return;
			}

			EditorGUILayout.LabelField(platformProperties.name, Style.LabelBigStyle);

			foreach (DistributionPlatform distributionPlatform in platformProperties.getActiveDistributionPlatformList())
			{
				Utils.BeginContents();
				{
					EditorGUILayout.LabelField("Distribution Platform: " + distributionPlatform.name, Style.LabelVersionStyle);

					foreach (PlatformArchitecture platformArchitecture in platformProperties.getActivePlatformArchitectureList())
					{
						foreach (ITextureCompression textureCompression in m_platform.getPlatformProperties().getActiveTextureCompressionList())
						{
							Utils.BeginContents();
							{
								if (platformProperties.isUsingPlatformArchitecture())
								{
									EditorGUILayout.BeginHorizontal();
									EditorGUILayout.LabelField("Architecture", GUILayout.Width(143f));
									EditorGUILayout.LabelField(platformArchitecture.name);
									EditorGUILayout.EndHorizontal();
								}

								if (platformProperties.isUsingTextureCompression())
								{
									EditorGUILayout.BeginHorizontal();
									EditorGUILayout.LabelField("Texture Compression", GUILayout.Width(143f));
									EditorGUILayout.LabelField(textureCompression.getName());
									EditorGUILayout.EndHorizontal();
								}

								if (m_platformAdditionalRenderer != null)
								{
									m_platformAdditionalRenderer.drawAdditionalBuildSummary(textureCompression, bundleVersion);
								}

								EditorGUILayout.LabelField(platformProperties.getBuildPath(advancedSettings, releaseType, m_platform, distributionPlatform, platformArchitecture, textureCompression, DateTime.Now));
							}
							Utils.EndContents();
							GUILayout.Space(5f);
						}
					}
				}
				Utils.EndContents();
				GUILayout.Space(5f);
			}
		}
	}
}