using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformiOS : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}

		
		/*
		 * Constructor
		 */
		public PlatformiOS()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.iOS,
				"iOS",
				new List<DistributionPlatform> { },
				new List<PlatformArchitecture>
				{
					new PlatformArchitecture(null, "", BuildTarget.iPhone, true),
				},
				new List<ITextureCompression>() { new DefaultTextureCompression() }
			);
		}
		
		
		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			PlayerSettings.iPhoneBundleIdentifier = releaseType.bundleIdentifier;
			PlayerSettings.iOS.applicationDisplayName = releaseType.productName;
		}
		
		
		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			return fileName;
		}
		
		
		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			return null;
		}
	}
}