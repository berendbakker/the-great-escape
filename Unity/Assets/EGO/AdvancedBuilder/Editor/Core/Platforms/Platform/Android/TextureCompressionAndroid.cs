using UnityEditor;
using UnityEngine;
using System;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class TextureCompressionAndroid : ITextureCompression
	{
		/*
		 * Determine if the texture compression will be used
		 */
		[SerializeField]
		private bool m_isActive;
		
		public bool isActive
		{
	        get { return m_isActive; }
			set { m_isActive = value; }
	    }
		
		
		/*
		 * Return the Android Build Sub Target
		 */
		public readonly AndroidBuildSubtarget subTarget;
		
		
		/*
		 * The version code prefix
		 */
		public readonly int versionCodePrefix;
		
		
		/*
		 * The texture compression name
		 */
		public string getName()
		{
			return subTarget.ToString();
		}
		
		
		public TextureCompressionAndroid(AndroidBuildSubtarget subTarget, int versionCodePrefix, bool isActive = false)
		{
			this.subTarget = subTarget;
			this.versionCodePrefix = versionCodePrefix;
			this.m_isActive = isActive;
		}
	}
}