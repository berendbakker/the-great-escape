#if !UNITY_4_0 && !UNITY_4_1
using UnityEditor;
using UnityEngine;
using System;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class TextureCompressionBlackBerry : ITextureCompression
	{
		/*
		 * Determine if the texture compression will be used
		 */
		[SerializeField]
		private bool m_isActive;
		
		public bool isActive
		{
	        get { return m_isActive; }
			set { m_isActive = value; }
	    }
		
		
		/*
		 * Return the BlackBerry Build Sub Target
		 */
		public readonly BlackBerryBuildSubtarget subTarget;
		
		
		/*
		 * The texture compression name
		 */
		public string getName()
		{
			return subTarget.ToString();
		}
		
		
		public TextureCompressionBlackBerry(BlackBerryBuildSubtarget subTarget, bool isActive = false)
		{
			this.subTarget = subTarget;
			this.m_isActive = isActive;
		}
	}
}
#endif