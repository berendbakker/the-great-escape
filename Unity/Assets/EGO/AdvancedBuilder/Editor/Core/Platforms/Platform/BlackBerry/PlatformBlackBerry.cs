#if !UNITY_4_0 && !UNITY_4_1
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformBlackBerry : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}
		
		
		/*
		 * List of supported texture compression
		 */
		[SerializeField]
		private List<TextureCompressionBlackBerry> m_textureCompressionList = new List<TextureCompressionBlackBerry>()
		{
			new TextureCompressionBlackBerry(BlackBerryBuildSubtarget.PVRTC),
			new TextureCompressionBlackBerry(BlackBerryBuildSubtarget.ATC),
			new TextureCompressionBlackBerry(BlackBerryBuildSubtarget.ETC),
			new TextureCompressionBlackBerry(BlackBerryBuildSubtarget.Generic, true),
		};


		/*
		 * BlackBerry Build types
		 */
		private string m_localBuildType = "Local";
		private string m_signedBuildType = "Signed";
		
		
		/*
		 * Constructor
		 */
		public PlatformBlackBerry()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.BlackBerry,
				"BlackBerry",
				new List<DistributionPlatform> { },
				new List<PlatformArchitecture>
				{
					#if UNITY_4_2 || UNITY_4_3 || UNITY_4_4
					new PlatformArchitecture(m_localBuildType, ".bar", BuildTarget.BB10, true),
					new PlatformArchitecture(m_signedBuildType, ".bar", BuildTarget.BB10),
					#else
					new PlatformArchitecture(m_localBuildType, ".bar", BuildTarget.BlackBerry, true),
					new PlatformArchitecture(m_signedBuildType, ".bar", BuildTarget.BlackBerry),
					#endif
				},
				m_textureCompressionList.Cast<ITextureCompression>().ToList()
			);
		}
		
		
		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			TextureCompressionBlackBerry textureCompressionBlackBerry = (TextureCompressionBlackBerry)textureCompression;

			if (platformArchitecture.name.Equals(m_localBuildType))
			{
				EditorUserBuildSettings.blackberryBuildType = BlackBerryBuildType.Debug;
			}
			else if (platformArchitecture.name.Equals(m_signedBuildType))
			{
				EditorUserBuildSettings.blackberryBuildType = BlackBerryBuildType.Submission;
			}

			EditorUserBuildSettings.blackberryBuildSubtarget = textureCompressionBlackBerry.subTarget;
		}


		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			#if UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_6
			return fileName.Replace(" ", string.Empty);
			#else
			return fileName;
			#endif
		}
		
		
		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			return null;
		}
	}
}
#endif