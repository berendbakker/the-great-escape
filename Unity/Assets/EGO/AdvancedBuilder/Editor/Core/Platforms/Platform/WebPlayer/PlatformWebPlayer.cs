using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class PlatformWebPlayer : IPlatform
	{
		/*
		 * Platform common properties
		 */
		[SerializeField] private PlatformProperties m_platformProperties;
		
		public PlatformProperties getPlatformProperties()
		{
			return m_platformProperties;
		}
		
		
		/*
		 * Constructor
		 */
		public PlatformWebPlayer()
		{
			m_platformProperties = new PlatformProperties(
				PlatformType.WebPlayer,
				"WebPlayer",
				new List<DistributionPlatform>
				{
					new DistributionPlatform("My Website", true),
					new DistributionPlatform("Kongregate", true),
				},
				new List<PlatformArchitecture>
				{
					new PlatformArchitecture("WebPlayer", "", BuildTarget.WebPlayer, true),
					new PlatformArchitecture("WebPlayer streamed", "", BuildTarget.WebPlayerStreamed),
				},
				new List<ITextureCompression>() { new DefaultTextureCompression() }
			);
		}
		
		
		/*
		 * Set up additional parameters
		 */
		public void setupAdditionalParameters(ProductParameters productParameters, ReleaseType releaseType, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
		}
		
		
		/*
		 * Format final file name
		 */
		public string formatFinalFileName(string fileName)
		{
			return fileName;
		}
		
		
		/*
		 * Return specific platform errors
		 */
		public string getPlatformError()
		{
			return null;
		}
	}
}