namespace com.pygmymonkey.tools
{
	public interface IPlatformAdditionalRenderer
	{
		/*
		 * Draw additional build summary
		 */
		void drawAdditionalBuildSummary(ITextureCompression textureCompression, string bundleVersion);
	}
}