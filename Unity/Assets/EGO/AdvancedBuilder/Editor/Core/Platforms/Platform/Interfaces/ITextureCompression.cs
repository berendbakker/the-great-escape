namespace com.pygmymonkey.tools
{
	public interface ITextureCompression
	{
		bool isActive { get; set; }
    	string getName();
	}
}