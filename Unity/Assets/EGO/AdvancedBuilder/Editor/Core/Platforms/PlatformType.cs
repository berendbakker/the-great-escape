namespace com.pygmymonkey.tools
{
	public enum PlatformType
	{
		Android = 0,
		iOS,
		WebPlayer,
		Windows,
		Mac,
		Linux,
		#if !UNITY_4_0 && !UNITY_4_1
		WindowsPhone8,
		WindowsStore8,
		BlackBerry,
		#endif
	}
}