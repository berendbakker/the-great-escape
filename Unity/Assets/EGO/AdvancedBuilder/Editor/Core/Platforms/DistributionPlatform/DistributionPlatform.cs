using UnityEngine;
using System;
using System.Text.RegularExpressions;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class DistributionPlatform
	{
		/*
		 * Determine if the distributionplatform will be used
		 */
		public bool isActive;
		
		
		/*
		 * The distributionplatform name
		 */
		public string name;
		
		
		public DistributionPlatform(string name, bool isActive = false)
		{
			this.name = name;
			this.isActive = isActive;
		}

		public bool isNameValid()
		{
			return !string.IsNullOrEmpty(name) && Regex.IsMatch(name, @"^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$");
		}
		
		public string getFormattedName()
		{
			return name.Replace(" ", string.Empty);
		}
	}
}