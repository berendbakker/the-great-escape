using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class AdvancedBuilder : ScriptableObject
	{
		/*
		 * Product parameters
		 */
		[SerializeField] private ProductParameters m_productParameters = new ProductParameters();

		public ProductParameters getProductParameters()
		{
			return m_productParameters;
		}


		/*
		 * Release types
		 */
		[SerializeField] private ReleaseTypes m_releaseTypes = new ReleaseTypes();

		public ReleaseTypes getReleaseTypes()
		{
			return m_releaseTypes;
		}

		
		/*
		 * Suppoted platforms
		 */
		[SerializeField] private Platforms m_platforms = new Platforms();

		public Platforms getPlatforms()
		{
			return m_platforms;
		}


		/*
		 * Advanced settings
		 */
		[SerializeField] private AdvancedSettings m_advancedSettings = new AdvancedSettings();

		public AdvancedSettings getAdvancedSettings()
		{
			return m_advancedSettings;
		}

		
		/*
		 * We compute the total number of build
		 */
		public int getTotalBuildCount()
		{
			int buildCount = 0;

			foreach (ReleaseType releaseType in m_releaseTypes.getReleaseTypeList())
			{
				if (releaseType.isActive)
				{
					foreach (IPlatform platform in m_platforms.platformDictionary.Values)
					{
						buildCount += platform.getPlatformProperties().getBuildCount();
					}
				}
			}
			
			return buildCount;
		}


		public static void PerformBuild()
		{
			AdvancedBuilder advancedBuilder = (AdvancedBuilder)AssetDatabase.LoadAssetAtPath("Assets/AdvancedBuilder/Editor/AdvancedBuilder.asset", typeof(AdvancedBuilder));

			AppParametersHelper.SaveBuildTarget();

			DateTime buildDate = DateTime.Now;
			foreach (ReleaseType releaseType in advancedBuilder.getReleaseTypes().getReleaseTypeList().Where(x => x.isActive))
			{
				PlatformBuilder platformBuilder = new PlatformBuilder(advancedBuilder, releaseType, buildDate);
				foreach (IPlatform platform in advancedBuilder.getPlatforms().platformDictionary.Values)
				{
					platformBuilder.performBuild(platform);
				}
			}

			AppParametersHelper.RestoreBuildTarget();
		}
	}
}
