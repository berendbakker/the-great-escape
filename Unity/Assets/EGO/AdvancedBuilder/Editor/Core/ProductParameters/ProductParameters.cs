﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	[Serializable]
	public class ProductParameters
	{
		/*
		 * Bundle version
		 */
		public string bundleVersion = "0.0.1";
	}
}