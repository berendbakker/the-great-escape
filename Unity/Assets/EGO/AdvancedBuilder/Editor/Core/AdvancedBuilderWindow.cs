using UnityEditor;
using UnityEngine;
using System.IO;

namespace com.pygmymonkey.tools
{
	public class AdvancedBuilderWindow : EditorWindow
	{
	    public static string VERSION_NAME = "1.2.6";
		
		private AdvancedBuilder m_advancedBuilder;
		private AppParameters m_appParameters;
		private Vector2 m_scrollPosition;
		
	    // ===================================================================================
	    // UNITY METHODS ---------------------------------------------------------------------
		
		[MenuItem("Window/Advanced Builder")] 
	    private static void ShowWindow()
	    {
	        AdvancedBuilderWindow window = (AdvancedBuilderWindow)EditorWindow.GetWindow(typeof(AdvancedBuilderWindow), false, "Advanced Builder");
			window.minSize = new Vector2(440, 500);
	    }
		
	    private void OnEnable()
	    {
			hideFlags = HideFlags.HideAndDontSave;
			
			loadScriptableObject();
	    }
	
	    private void OnFocus()
	    {
	        Repaint();
	    }
	
	    private void OnSelectionChange()
	    {
	        Repaint();
	    }
		
		
		
	    // ===================================================================================
	    // GUI METHODS -----------------------------------------------------------------------
		
		private void OnGUI()
	    {
			Style.InitGUI();

			if (m_advancedBuilder == null || m_appParameters == null)
			{
				loadScriptableObject();
				return;
			}
			
			bool wasBuildProcessLaunched = false;
			m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition);
			{
				GUILayout.Space(10f);
				
				EditorGUILayout.LabelField("Advanced Builder", Style.LabelTitleStyle);
				EditorGUILayout.LabelField("Version " + VERSION_NAME, Style.LabelVersionStyle);
				
				wasBuildProcessLaunched = new AdvancedBuilderRenderer(m_advancedBuilder).drawInspector();
				
				if (GUI.changed)
				{
					EditorUtility.SetDirty(m_advancedBuilder);
				}
				
				GUILayout.Space(10f);
			}
			if (!wasBuildProcessLaunched)
			{
				EditorGUILayout.EndScrollView();
			}
	    }
		
		
		
		// ===================================================================================
	    // ADVANCED BUILDER METHODS -----------------------------------------------------------------------
		
		private void loadScriptableObject()
		{
			m_advancedBuilder = createScriptableObject<AdvancedBuilder>("Assets/AdvancedBuilder/Editor/AdvancedBuilder.asset");
			m_appParameters = createScriptableObject<AppParameters>("Assets/AdvancedBuilder/AppParameters/Resources/AppParameters.asset");
		}

		private T createScriptableObject<T>(string path) where T : ScriptableObject
		{
			T scriptableObject = (T)AssetDatabase.LoadAssetAtPath(path, typeof(T));
			
			if (scriptableObject != null)
			{
				return scriptableObject;
			}
			
			scriptableObject = ScriptableObject.CreateInstance<T>();
			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path);

			string directoryPath = path.Substring(0, path.LastIndexOf("/"));
			if (!Directory.Exists(directoryPath))
			{
				Directory.CreateDirectory(directoryPath);
				return null;
			}

			AssetDatabase.CreateAsset(scriptableObject, assetPathAndName);
			Utils.RefreshAssets();
			
			return scriptableObject;
		}
	}
}