﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	public class AdvancedBuilderRenderer
	{
		/*
		 * AdvancedBuilder data
		 */
		private AdvancedBuilder m_advancedBuilder;
		
		
		/*
		 * Constructor
		 */
		public AdvancedBuilderRenderer(AdvancedBuilder advancedBuilder)
		{
			m_advancedBuilder = advancedBuilder;
		}


		public bool drawInspector()
		{
			ErrorReporter errorReporter = new ErrorReporter();
			
			// -------------------
			// Product parameters
			// -------------------
			ProductParametersRenderer productParametersRenderer = new ProductParametersRenderer(m_advancedBuilder.getProductParameters());
			productParametersRenderer.checkWarningsAndErrors(errorReporter);

			if (Utils.DrawHeader("Product Parameters", "ab_product_parameters"))
			{
				Utils.BeginContents();
				productParametersRenderer.drawInspector();
				Utils.EndContents();
			}
			
			
			
			// --------------
			// Release Types
			// --------------
			ReleaseTypesRenderer releaseTypesRenderer = new ReleaseTypesRenderer(m_advancedBuilder.getReleaseTypes());
			releaseTypesRenderer.checkWarningsAndErrors(errorReporter);

			if (Utils.DrawHeader("Release Types", "ab_release_types"))
			{
				Utils.BeginContents();
				releaseTypesRenderer.drawInspector();
				Utils.EndContents();
			}
			
			
			
			// ----------
			// Platforms
			// ----------
			PlatformsRenderer platformsRenderer = new PlatformsRenderer(m_advancedBuilder.getPlatforms());
			platformsRenderer.checkWarningsAndErrors(errorReporter);

			if (Utils.DrawHeader("Platforms", "ab_platforms"))
			{
				Utils.BeginContents();
				platformsRenderer.drawInspector();
				Utils.EndContents();
			}



			// ------------------
			// Advanced Settings
			// ------------------
			AdvancedSettingsRenderer advancedSettingsRenderer = new AdvancedSettingsRenderer(m_advancedBuilder.getAdvancedSettings());
			advancedSettingsRenderer.checkWarningsAndErrors(errorReporter);

			if (Utils.DrawHeader("Advanced Settings", "ab_advanced_settings"))
			{
				Utils.BeginContents();
				advancedSettingsRenderer.drawInspector();
				Utils.EndContents();
			}



			// ------------------
			// App Configuration
			// ------------------
			ProjectConfigurationRenderer projectConfigurationRenderer = new ProjectConfigurationRenderer(m_advancedBuilder);
			projectConfigurationRenderer.checkWarningsAndErrors(errorReporter);

			if (Utils.DrawHeader("Project Configuration", "ab_project_configuration"))
			{
				Utils.BeginContents();
				projectConfigurationRenderer.drawInspector();
				Utils.EndContents();
			}
			
			
			
			// ------------------
			// Warnings / Errors
			// ------------------
			if (!UnityEditorInternal.InternalEditorUtility.HasPro())
			{
				errorReporter.addError("Sorry, Advanced Builder requires Unity Pro. The multiple build functionality is disabled. You can, however, manually select the configuration you want in the 'Project Configuration' window and then build like you usually do via Build Settings."
				+ "\n\n" + "Note that without Unity Pro, Advanced Builder only allows you to manage multiple configurations, it will not use things in 'Product Parameters' or 'Advanced Settings' and Custom Build Scripts are not available.");
			}

			if (errorReporter.getWarningCount() > 0 || errorReporter.getErrorCount() > 0)
			{
				if (Utils.DrawHeader("Warnings & Errors", "ab_warnings_errors", true))
				{
					Utils.BeginContents();
					
					foreach (string iMessage in errorReporter.getWarningList())
					{
						EditorGUILayout.HelpBox(iMessage, MessageType.Warning, true);
					}
					
					foreach (string iMessage in errorReporter.getErrorList())
					{
						EditorGUILayout.HelpBox(iMessage, MessageType.Error, true);
					}
					
					Utils.EndContents();
				}
			}
			
			
			
			// ------------------
			// Perform Build
			// ------------------
			if (Utils.DrawHeader("Perform Build", "ab_perform_build"))
			{
				Utils.BeginContents();
				{
					bool isBuildAllowed = m_advancedBuilder.getTotalBuildCount() != 0 && errorReporter.getErrorCount() <= 0;

					
					/*
					 * Printing a summary of all the builds
					 */
					if (isBuildAllowed)
					{
						List<ReleaseType> activeReleaseTypeList = m_advancedBuilder.getReleaseTypes().getReleaseTypeList().Where(x => x.isActive).ToList();
						for (int i = 0; i < activeReleaseTypeList.Count; i++)
						{
							ReleaseType releaseType = activeReleaseTypeList[i];
							
							if (Utils.DrawHeader(releaseType.name + " (" + releaseType.productName + " - " + releaseType.bundleIdentifier + " - " + m_advancedBuilder.getProductParameters().bundleVersion + ")", true))
							{
								Utils.BeginContents();
								{
									foreach (IPlatformRenderer platformRenderer in platformsRenderer.getActivePlatformRenderers())
									{
										platformRenderer.drawBuildSummary(m_advancedBuilder.getAdvancedSettings(), releaseType, m_advancedBuilder.getProductParameters().bundleVersion);
									}
								}
								Utils.EndContents();
							}
							
							if (i < activeReleaseTypeList.Count - 1)
							{
								GUILayout.Space(10f);
							}
						}
					}
					
					
					/*
					 * Perform the build
					 */
					GUI.enabled = isBuildAllowed;
					
					if (Utils.DrawBigButton("Perform a total of " + m_advancedBuilder.getTotalBuildCount() + " builds on " + m_advancedBuilder.getPlatforms().getPlatformActiveCount() + " platforms", Color.green))
					{
						AdvancedBuilder.PerformBuild();
						return true;
					}
					
					GUI.enabled = true;
						
						
					/*
					 * Button to open build folder
					 */
					if (Utils.DrawBigButton("Open Build folder"))
					{
						string buildFolderPath = Application.dataPath.Replace("Assets", "Builds");
						
						if (!Directory.Exists(buildFolderPath))
						{
							Directory.CreateDirectory(buildFolderPath);
						}
						
						EditorUtility.OpenWithDefaultApp(buildFolderPath);
					}
					
					GUILayout.Space(5f);
				}
				Utils.EndContents();
			}

			return false;
		}
	}
}
