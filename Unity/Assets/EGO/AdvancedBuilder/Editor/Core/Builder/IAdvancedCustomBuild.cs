﻿namespace com.pygmymonkey.tools
{
	public interface IAdvancedCustomBuild
	{
		void OnPreBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression);
		void OnPostBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression);
	}
}
