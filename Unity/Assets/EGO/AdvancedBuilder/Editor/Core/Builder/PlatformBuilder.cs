using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.pygmymonkey.tools
{
	internal class PlatformBuilder
	{
		private readonly AdvancedBuilder m_advancedBuilder;
		private readonly ReleaseType m_releaseType;
		private readonly DateTime m_buildDate;
		private readonly IAdvancedCustomBuild m_advancedCustomBuild;

		public PlatformBuilder(AdvancedBuilder advancedBuilder, ReleaseType releaseType, DateTime buildDate)
		{
			m_advancedBuilder = advancedBuilder;
			m_releaseType = releaseType;
			m_buildDate = buildDate;

			if (m_advancedBuilder.getAdvancedSettings().customBuildMonoScript != null)
			{
				m_advancedCustomBuild = (IAdvancedCustomBuild)System.Activator.CreateInstance(m_advancedBuilder.getAdvancedSettings().customBuildMonoScript.GetClass());
			}
		}

		public void performBuild(IPlatform platform)
		{
			PlatformProperties platformProperties = platform.getPlatformProperties();
			if (!platformProperties.isActive)
			{
				return;
			}

			string buildError = platform.getPlatformError();
			if (!string.IsNullOrEmpty(buildError))
			{
				Debug.LogError("Build for the platform: " + platformProperties.name + " was not processed: " + buildError);
				return;
			}

			foreach (DistributionPlatform distributionPlatform in platformProperties.getActiveDistributionPlatformList())
			{
				foreach (PlatformArchitecture platformArchitecture in platformProperties.getActivePlatformArchitectureList())
				{
					foreach (ITextureCompression textureCompression in platformProperties.getActiveTextureCompressionList())
					{
						/*
						 * Update AppParameters file
						 */
						AppParametersHelper.SaveParameters();
						AppParametersHelper.GenerateAppParameters(m_advancedBuilder.getProductParameters(), m_releaseType, platformProperties, distributionPlatform, platformArchitecture, textureCompression);


						/*
						 * Set Player Settings
						 */
						PlayerSettings.bundleIdentifier = m_releaseType.bundleIdentifier;
						PlayerSettings.bundleVersion = m_advancedBuilder.getProductParameters().bundleVersion;
						PlayerSettings.productName = m_releaseType.productName;

						platform.setupAdditionalParameters(m_advancedBuilder.getProductParameters(), m_releaseType, platformArchitecture, textureCompression);


						/*
						 * Get destination path and file name
						 */
						string buildDestinationPath = platformProperties.getBuildDestinationPath(m_advancedBuilder.getAdvancedSettings(), m_releaseType, distributionPlatform, platformArchitecture, m_buildDate);
						string buildFileName = platformProperties.getBuildFileName(m_releaseType, platform, platformArchitecture, textureCompression);


						/*
						 * Perform the build
						 */
						performPreBuild(m_releaseType, platformProperties.platformType, distributionPlatform, platformArchitecture, textureCompression);
						bool success = performBuild(platformProperties, platformArchitecture.buildTarget, buildDestinationPath, buildFileName, m_advancedBuilder.getAdvancedSettings());
						performPostBuild(m_releaseType, platformProperties.platformType, distributionPlatform, platformArchitecture, textureCompression);


						// Hack to prevent violation access when editing AppParameters.cs script
						System.Threading.Thread.Sleep(500);
						//TODO: Can we remove that now?


						/*
						 * Restore AppParameters
						 */
						if (success)
						{
							AppParametersHelper.RestoreParameters();
						}

						EditorUtility.ClearProgressBar();
					}
				}
			}
		}

		private void performPreBuild(ReleaseType releaseType, PlatformType platformType, DistributionPlatform distributionPlatform, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			if (m_advancedCustomBuild != null)
			{
				EditorUtility.DisplayProgressBar("Advanced Builder", "Doing custom pre build stuff...", 0.0f);
				m_advancedCustomBuild.OnPreBuild(releaseType.getFormattedName(), platformType.ToString(), distributionPlatform.getFormattedName(), platformArchitecture.getFormattedName(), textureCompression.getName());
			}
		}

		private bool performBuild(PlatformProperties platformProperties, BuildTarget buildTarget, string buildDestinationPath, string fileName, AdvancedSettings advancedSettings)
		{
			/*
			 * Create the build folder if it does not exist
			 */
			string buildDirectory = Application.dataPath.Replace("Assets", "Builds");
			Directory.CreateDirectory(buildDirectory);
			
			
			/*
			 * Create the subfolder that will contain the final build
			 */
			string destinationFolder = buildDirectory + "/" + buildDestinationPath;
			Directory.CreateDirectory(destinationFolder);

			string finalBuildPath = destinationFolder + fileName;
			
			
			/*
			 * Before building, we refresh all assets
			 */
			Utils.RefreshAssets();
			
			/*
			 * Start the build through Unity process
			 */
			EditorUtility.DisplayProgressBar("Advanced Builder", "Building " + finalBuildPath.Replace("/", " - ").Replace(buildDirectory, string.Empty) + "...", 0.5f);
			
			EditorUserBuildSettings.SwitchActiveBuildTarget(buildTarget);
			
			// Set Build Options
			BuildOptions buildOptions = BuildOptions.None;
			
			if (advancedSettings.openBuildFolder)
			{
				buildOptions |= BuildOptions.ShowBuiltPlayer;
			}
			
			if (advancedSettings.isDevelopmentBuild)
			{
				buildOptions |= BuildOptions.Development;
			}
			
			if (advancedSettings.shouldAutoconnectProfiler)
			{
				buildOptions |= BuildOptions.ConnectWithProfiler;
			}
			
			if (advancedSettings.shouldAutoRunPlayer)
			{
				buildOptions |= BuildOptions.AutoRunPlayer;
			}
			
			if (advancedSettings.allowDebugging)
			{
				buildOptions |= BuildOptions.AllowDebugging;
			}
			
			EditorUserBuildSettings.appendProject = advancedSettings.appendProject;
			
			if (advancedSettings.useSymlinkLibraries)
			{
				buildOptions |= BuildOptions.SymlinkLibraries;
			}
			
			// Build Player
			string error = BuildPipeline.BuildPlayer(getScenePaths(), finalBuildPath, buildTarget, buildOptions);
			if (error != string.Empty)
			{
				Debug.LogError("Error:" + error);
				
				EditorUtility.ClearProgressBar();
				return false;
			}
			
			EditorUtility.DisplayProgressBar("Advanced Builder", "Doing post build stuff...", 1.0f);
			
			
			/*
			 * After building, we refresh all assets
			 */
			Utils.RefreshAssets();
			EditorUtility.ClearProgressBar();
			
			return true;
		}

		private void performPostBuild(ReleaseType releaseType, PlatformType platformType, DistributionPlatform distributionPlatform, PlatformArchitecture platformArchitecture, ITextureCompression textureCompression)
		{
			if (m_advancedCustomBuild != null)
			{
				EditorUtility.DisplayProgressBar("Advanced Builder", "Doing custom build stuff...", 1.0f);
				m_advancedCustomBuild.OnPostBuild(releaseType.getFormattedName(), platformType.ToString(), distributionPlatform.getFormattedName(), platformArchitecture.getFormattedName(), textureCompression.getName());
			}
		}

		private string[] getScenePaths()
		{
			return EditorBuildSettings.scenes.Where(x => x.enabled).Select(x => x.path).ToArray();
		}
	}
}
