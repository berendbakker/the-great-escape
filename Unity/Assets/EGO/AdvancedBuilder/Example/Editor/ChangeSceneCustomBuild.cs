using UnityEngine;
using UnityEditor;
using com.pygmymonkey.tools;

// This class MUST be in an "Editor" folder!
// This is an example of a custom build script that will select the scenes you want to include in your builds depending on the release type.
public class ChangeSceneCustomBuild : IAdvancedCustomBuild
{
	// We first define a variable that will save the current scenes.
	private EditorBuildSettingsScene[] m_savedBuildSettingsSceneArray;


	/// <summary>
	/// Callback method that is called before each build
	/// </summary>
	public void OnPreBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression)
	{
		// We save the current scenes, so we can restore them later
		m_savedBuildSettingsSceneArray = EditorBuildSettings.scenes;

		// Depending on the parameters you want, here the release type, we specify which scenes we want to include
		if (releaseType.Equals("Dev"))
		{
			updateUsedScenes(new string[] {
				"Assets/AdvancedBuilder/Example/Scene/Demo1.unity"
			});
		}
		else if (releaseType.Equals("Beta"))
		{
			// As we're using a string array as parameter, we can pass multiple scenes
			updateUsedScenes(new string[] {
				"Assets/AdvancedBuilder/Example/Scene/Demo2.unity",
				"Assets/AdvancedBuilder/Example/Scene/Demo1.unity"
			});
		}
	}


	/// <summary>
	/// Callback method that is called after each build
	/// </summary>
	public void OnPostBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression)
	{
		// We restore the scenes that were used before the build
		EditorBuildSettings.scenes = m_savedBuildSettingsSceneArray;

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}


	/// <summary>
	/// Helper method to easily update scenes in Build Settings
	/// </summary>
	private void updateUsedScenes(string[] scenePathArray)
	{
		EditorBuildSettingsScene[] sceneArray = new EditorBuildSettingsScene[scenePathArray.Length];
		
		for (int i = 0; i < scenePathArray.Length; i++)
		{
			sceneArray[i] = new EditorBuildSettingsScene(scenePathArray[i], true);
		}
		
		EditorBuildSettings.scenes = sceneArray;

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}
}
