using UnityEngine;
using UnityEditor;
using com.pygmymonkey.tools;

// This class MUST be in an "Editor" folder!
public class MyCustomBuild : IAdvancedCustomBuild
{
	/// <summary>
	/// Callback method that is called before each build
	/// </summary>
	public void OnPreBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression)
	{
		Debug.Log("Do stuff before build");
	}

	/// <summary>
	/// Callback method that is called after each build
	/// </summary>
	public void OnPostBuild(string releaseType, string platformType, string distributionPlatform, string platformArchitecture, string textureCompression)
	{
		Debug.Log("Do stuff after build");
	}
}
