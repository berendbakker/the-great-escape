﻿using UnityEngine;
using com.pygmymonkey.tools;

public class Demo1 : MonoBehaviour
{
	void Start()
	{
		if (AppParameters.Get.platformType.Equals("Android"))
		{
			// Do something Android specific
		}
		else if (AppParameters.Get.platformType.Equals("iOS"))
		{
			// Do something iOS specific
		}

		switch (AppParameters.Get.releaseType)
		{
		case "Dev":
			// Do something with the dev version of your app
			break;
			
		case "Beta":
			// Do something with the beta version of your app
			break;
			
		case "Release":
			// Do something with the release version of your app
			break;
		}
	}

	void OnGUI()
	{
		int paddingX = 5;
		int paddingY = 5;
		int width = 200;
		int height = 22;

		GUI.Label(new Rect(paddingX, paddingY, width, height), "Release type:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.releaseType);
		
		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Platform type:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.platformType);
		
		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Distribution platform:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.distributionPlatform);
		
		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Platform architecture:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.platformArchitecture);
		
		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Texture compression:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.textureCompression);

		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Product name:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.productName);

		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Bundle identifier:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.bundleIdentifier);

		paddingY += height;
		GUI.Label(new Rect(paddingX, paddingY, width, height), "Bundle version:");
		GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.bundleVersion);

		if (AppParameters.Get.platformType.Equals("Android"))
		{
			paddingY += height;
			GUI.Label(new Rect(paddingX, paddingY, width, height), "Texture Compression:");
			GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.textureCompression);
		}

		if (AppParameters.Get.platformType.Equals("Windows"))
		{
			paddingY += height;
			GUI.Label(new Rect(paddingX, paddingY, width, height), "Platform Architecture:");
			GUI.Label(new Rect(paddingX + width, paddingY, width, height), AppParameters.Get.platformArchitecture);
		}
	}
}
