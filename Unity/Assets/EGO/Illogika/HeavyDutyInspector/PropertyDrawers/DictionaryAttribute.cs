﻿//----------------------------------------------
//            Heavy-Duty Inspector
//      Copyright © 2014 - 2015  Illogika
//----------------------------------------------
#if !UNITY_4_0 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_4
using UnityEngine;

namespace HeavyDutyInspector
{

	public class DictionaryAttribute : PropertyAttribute {

		public string valuesListName
		{
			get;
			private set;
		}

		public KeywordsConfig keywordConfig
		{
			get;
			private set;
		}

		public DictionaryAttribute(string valuesListName)
		{
			keywordConfig = null;
			this.valuesListName = valuesListName;
		}

		public DictionaryAttribute(string valuesListName, string keywordsConfigFile)
		{
			keywordConfig = Resources.Load(keywordsConfigFile) as KeywordsConfig;
			this.valuesListName = valuesListName;
		}
	}	
}
#endif
	