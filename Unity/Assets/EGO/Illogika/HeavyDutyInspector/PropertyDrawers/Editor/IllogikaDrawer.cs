//----------------------------------------------
//            Heavy-Duty Inspector
//      Copyright © 2013 - 2015  Illogika
//----------------------------------------------

using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace HeavyDutyInspector
{

	public class IllogikaDrawer : PropertyDrawer {

		private static readonly Color objectNullColor = Color.yellow;
		private static readonly Color objectSelfColor = Color.white;
		private static readonly Color objectOtherColor = Color.cyan;

		private bool doOnlyOnce;

	#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2
		protected bool CanDisplayVariable(SerializedProperty prop)
		{
			return 	prop.propertyType == SerializedPropertyType.Boolean ||
				prop.propertyType == SerializedPropertyType.Color ||
					prop.propertyType == SerializedPropertyType.Float ||
					prop.propertyType == SerializedPropertyType.Integer ||
					prop.propertyType == SerializedPropertyType.ObjectReference ||
					prop.propertyType == SerializedPropertyType.Rect ||
					prop.propertyType == SerializedPropertyType.String ||
					prop.propertyType == SerializedPropertyType.Vector2 ||
					prop.propertyType == SerializedPropertyType.Vector3;
		}

		public override float GetPropertyHeight (SerializedProperty prop, GUIContent label)
		{
			switch(prop.propertyType)
			{
			case SerializedPropertyType.Rect:
				return base.GetPropertyHeight(prop, label) * 3;
			case SerializedPropertyType.Vector2:
				return base.GetPropertyHeight(prop, label) * 2;
			case SerializedPropertyType.Vector3:
				return base.GetPropertyHeight(prop, label) * 2;
			default:
				return base.GetPropertyHeight(prop, label);
			}
		}

		public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label)
		{			
			switch(prop.propertyType)
			{
			case SerializedPropertyType.Boolean:
				prop.boolValue = EditorGUI.Toggle(position, label, prop.boolValue);
				break;
			case SerializedPropertyType.Color:
				prop.colorValue = EditorGUI.ColorField(position, label, prop.colorValue);
				break;
			case SerializedPropertyType.Float:
				prop.floatValue = EditorGUI.FloatField(position, label, prop.floatValue);
				break;
			case SerializedPropertyType.Integer:
				prop.intValue = EditorGUI.IntField(position, label, prop.intValue);
				break;
			case SerializedPropertyType.ObjectReference:
				System.Type type = typeof(UnityEngine.Object);
				if(prop.objectReferenceValue != null)
					type = prop.objectReferenceValue.GetType();
				prop.objectReferenceValue = EditorGUI.ObjectField(position, label, prop.objectReferenceValue, type, true);
				break;
			case SerializedPropertyType.Rect:
				prop.rectValue = EditorGUI.RectField(position, label, prop.rectValue);
				break;
			case SerializedPropertyType.String:
				prop.stringValue = EditorGUI.TextField(position, label, prop.stringValue);
				break;
			case SerializedPropertyType.Vector2:
				prop.vector2Value = EditorGUI.Vector2Field(position, label.text, prop.vector2Value);
				break;
			case SerializedPropertyType.Vector3:
				prop.vector3Value = EditorGUI.Vector3Field(position, label.text, prop.vector3Value);
				break;
			default:
				break;
			}
			
		}
		
		protected void UnsupportedVariableWarning(string attributeName)
		{
			Debug.LogError(string.Format("You have used the {0} Attribute with a variable type that is not supported. Your variable will be hidden by default in the Inspector. To remove this warning, explicitly hide the variable in the Attribute's constructor. Refer to the documentation for a list of supported types, or upgrade to Unity 4.3, for which all variable types are supported.", attributeName));
			doOnlyOnce = true;
		}
	#else
		
		public override float GetPropertyHeight (SerializedProperty prop, GUIContent label)
		{
			if(prop.propertyType == SerializedPropertyType.Rect)
			{
				return base.GetPropertyHeight(prop, label) * 2;
			}
			
			return base.GetPropertyHeight(prop, label);
		}
		
	#endif

		protected void WrongVariableTypeWarning(string attributeName, string variableType)
		{
			if(!doOnlyOnce)
			{
				Debug.LogError(string.Format("The {0}Attribute is designed to be applied to {1} only!", attributeName, variableType));
				doOnlyOnce = true;
			}
		}

		private static Dictionary<string, GameObject> targetObjects = new Dictionary<string, GameObject>();

	#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2
		protected void OnComponentGUI (Rect position, SerializedProperty prop, GUIContent label, System.Type componentType, string fieldName, string[] requiredValues, string defaultObject, bool isPrefab, int rightOffset)
	#else
		protected void OnComponentGUI (Rect position, SerializedProperty prop, GUIContent label, string fieldName, string[] requiredValues, string defaultObject, bool isPrefab, int rightOffset)
	#endif
		{
			if(prop.propertyType != SerializedPropertyType.ObjectReference)
			{
				WrongVariableTypeWarning("ComponentSelection", "object references");
				return;
			}
			
			int originalIndentLevel = EditorGUI.indentLevel;

			GUIContent tempLabel = new GUIContent(label);
			tempLabel.tooltip = "1st Line : GameObject Reference. The GameObject on which to select a Component. This is not Serialized.\n\n2nd Line : The Selected Component. This is your actual Serialized variable.\n\n(GameObject reference is displayed in YELLOW color when the selected component is null, WHITE whe the selected component is on the same GameObject as this script and CYAN when the selected component is on another GameObject.)";
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), tempLabel);
			EditorGUI.indentLevel = 0;
			
			if(prop.hasMultipleDifferentValues)
			{
				position.height = GetPropertyHeight(prop, label);
				
				#if !UNITY_4_0 && !UNITY_4_1 && !UNITY_4_2
				if(fieldInfo.FieldType.IsArray || fieldInfo.FieldType.IsGenericType)
					position.x += 15;
				#endif
				EditorGUI.HelpBox(position, "Multi object editing is not supported for references with different values.", MessageType.Warning);
				return;
			}

			bool firstInit = false;
			if(!targetObjects.ContainsKey(prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath))
			{
				firstInit = true;
				targetObjects.Add(prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath, null);
			}
			
			// Set back the target game object if the drawed object has been deselected.
			if(prop.objectReferenceValue != null && (targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] == null || targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] != (prop.objectReferenceValue as Component).gameObject))
			{
				targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = (prop.objectReferenceValue as Component).gameObject;
			}
			
			if(firstInit && targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] == null)
			{
				firstInit = false;
				try
				{
					targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = string.IsNullOrEmpty(defaultObject) ? (prop.serializedObject.targetObject as MonoBehaviour).gameObject : isPrefab ? Resources.Load(defaultObject) as GameObject : GameObject.Find(defaultObject);
				}
				catch
				{
					targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = string.IsNullOrEmpty(defaultObject) ? null : isPrefab ? Resources.Load(defaultObject) as GameObject : null;
				}
			}
			
			if((prop.serializedObject.targetObject as ScriptableObject) != null)
			{
				if(targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] != null && !AssetDatabase.Contains(targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath]))
				   targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = null;
			}
			
			position.height /= 2;
			position.width -= rightOffset;
			
			EditorGUI.BeginChangeCheck();
			
			Color tempColor = GUI.color;
			try{
				GUI.color = targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] == (prop.serializedObject.targetObject as MonoBehaviour).gameObject ? objectSelfColor : objectOtherColor;
			}
			catch{
				GUI.color = objectOtherColor;
			}

			if(targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] == null)
				GUI.color = objectNullColor;

				targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = EditorGUI.ObjectField(position, targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath], typeof(GameObject), true) as GameObject;
			
			GUI.color = tempColor;

			if(targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] == null)
			{
				prop.objectReferenceValue = null;
				return;
			}

			position.width += rightOffset;

			#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2
			List<Component> components = targetObjects[prop.propertyPath].GetComponents(componentType).ToList();
			#else
			System.Type componentType;
			List<Component> components;
			if(fieldInfo.FieldType.IsArray)
				componentType = fieldInfo.FieldType.GetElementType();
			else if(fieldInfo.FieldType.IsGenericType)
				componentType = fieldInfo.FieldType.GetGenericArguments()[0];
			else
				componentType = fieldInfo.FieldType;

			components = targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath].GetComponents(componentType).ToList();
			#endif

			if(EditorGUI.EndChangeCheck())
			{
#if UNITY_4_0 
				Undo.RegisterUndo(prop.serializedObject.targetObjects, "Change Target Object");
#else
				Undo.RecordObjects(prop.serializedObject.targetObjects, "Change Target Object");
#endif
				prop.objectReferenceValue = null;

				foreach(Object obj in prop.serializedObject.targetObjects)
				{
					EditorUtility.SetDirty(obj);
				}
			}

			if(components.Contains(prop.serializedObject.targetObject as Component))
				components.Remove(prop.serializedObject.targetObject as Component);
			
			List<string> componentsNames = new List<string>();
			Dictionary<System.Type, int> componentsNumbers = new Dictionary<System.Type, int>();
			Dictionary<string, int> namedMonoBehavioursNumbers = new Dictionary<string, int>();
			List<Component> markedForDeletion = new List<Component>();

			foreach(Component component in components)
			{
				if(!componentsNumbers.ContainsKey(component.GetType()))
				{
					componentsNumbers.Add(component.GetType(), 1);
				}
				
				if(component is NamedMonoBehaviour)
				{ 
					if(string.IsNullOrEmpty((component as NamedMonoBehaviour).scriptName))
					{
						if(string.IsNullOrEmpty(fieldName))
						{
							componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString());
						}
						else
						{
							System.Object val = GetFieldOrPropertyValue(component, fieldName);

							if(requiredValues != null && requiredValues.Length > 0)
							{
								if(requiredValues.Contains(val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")))
									componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString() + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
								else
									markedForDeletion.Add(component);
							}
							else
							{
								componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString() + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
							}
						}
					}
					else
					{
						if(namedMonoBehavioursNumbers.ContainsKey((component as NamedMonoBehaviour).scriptName))
						{
							(component as NamedMonoBehaviour).scriptName += (" " + namedMonoBehavioursNumbers[(component as NamedMonoBehaviour).scriptName]++);
						}
						else
						{
							namedMonoBehavioursNumbers.Add((component as NamedMonoBehaviour).scriptName, 2);
						}

						if(string.IsNullOrEmpty(fieldName))
						{
							componentsNames.Add((component as NamedMonoBehaviour).scriptName);
						}
						else
						{
							System.Object val = GetFieldOrPropertyValue(component, fieldName);
							if(requiredValues != null && requiredValues.Length > 0)
							{
								if(requiredValues.Contains(val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")))
									componentsNames.Add((component as NamedMonoBehaviour).scriptName + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
								else
									markedForDeletion.Add(component);
							}
							else
							{
								componentsNames.Add((component as NamedMonoBehaviour).scriptName + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
							}
						}
						
					}
				}
				else
				{
					if(string.IsNullOrEmpty(fieldName))
					{
						componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString());
					}
					else
					{
						System.Object val = GetFieldOrPropertyValue(component, fieldName);
						if(requiredValues != null && requiredValues.Length > 0)
						{
							if(requiredValues.Contains(val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")))
								componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString() + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
							else
								markedForDeletion.Add(component);
						}
						else
						{
							componentsNames.Add(component.GetType().ToString().Replace("UnityEngine.", "") + " " + componentsNumbers[component.GetType()]++.ToString() + " (" + (val == null ? "null" : val.ToString().Replace(" (" + val.GetType().ToString() + ")", "")) + ")");
						}
					}
				}
			}

			foreach(Component component in markedForDeletion)
			{
				components.Remove(component);
			}

			if(components.Count == 0)
			{
				targetObjects[prop.serializedObject.targetObject.GetHashCode().ToString() + prop.propertyPath] = null;
				prop.objectReferenceValue = null;
				return;
			}

			components.Insert(0, null);
			#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2
			componentsNames.Insert(0, "None (" + componentType.ToString().Replace("UnityEngine.", "") + ")");
			#else
			componentsNames.Insert(0, "None (" + componentType.ToString().Replace("UnityEngine.", "") + ")");
			#endif
			
			position.y += position.height;
			
			int index = 0;
			
			try
			{
				index = components.IndexOf(prop.objectReferenceValue as Component);
			}
			catch
			{
				prop.objectReferenceValue = null;
			}

            try
            {
				if(index != 0 && typeof(NamedMonoBehaviour).IsAssignableFrom(components[index].GetType()))
					GUI.backgroundColor = (components[index] as NamedMonoBehaviour).scriptNameColor;

                prop.objectReferenceValue = components[EditorGUI.Popup(position, index, componentsNames.ToArray())];

				GUI.backgroundColor = Color.white;
            }
            catch
            {
			}
			
			EditorGUI.indentLevel = originalIndentLevel;
		}
		
		protected System.Object GetFieldOrPropertyValue(Component component, string fieldName)
		{
			if(component.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) != null)
				return component.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(component);
			else if(component.GetType().GetProperty(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) != null)
				return component.GetType().GetProperty(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(component, null);
			else
				Debug.LogError(string.Format("{0} does not contain a field or property named {1}!", component, fieldName));

			return "";
		}

		protected virtual void OnNamedMonoBehaviourGUI(Rect position, SerializedProperty property, GUIContent label, System.Type scriptType, int rightOffset)
		{
			position.height /= 2;
			
			int originalIndentLevel = EditorGUI.indentLevel;

			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID (FocusType.Passive), label);
			EditorGUI.indentLevel = 0;

			position.width -= rightOffset;

			if(property.hasMultipleDifferentValues)
			{
				EditorGUI.BeginChangeCheck();
				
				Object temp = EditorGUI.ObjectField(position, Resources.Load("-"), scriptType, true);

				if(EditorGUI.EndChangeCheck())
					property.objectReferenceValue = temp;

				position.width += rightOffset;

				position.y += position.height;
				Color color = GUI.color;
				GUI.color = new Color(1, 0.5f, 0);
				EditorGUI.LabelField(position, "[Multiple Values]");
				GUI.color = color;
			}
			else
			{
				property.objectReferenceValue = EditorGUI.ObjectField(position,(NamedMonoBehaviour)property.objectReferenceValue, scriptType, true);

				position.width += rightOffset;

				position.y += position.height;
				
				if(property.objectReferenceValue != null)
				{
					NamedMonoBehaviour monoBehaviour = (NamedMonoBehaviour)property.objectReferenceValue;
					Color color = GUI.color;
					GUI.color = monoBehaviour.scriptNameColor;
					EditorGUI.LabelField(position, string.Format("[{0}]", !string.IsNullOrEmpty(monoBehaviour.scriptName) ? monoBehaviour.scriptName : monoBehaviour.GetType().ToString() ) );
					GUI.color = color;
				}
			}
			
			EditorGUI.indentLevel = originalIndentLevel;
		}

		public void PropertyFieldIncludingSpecialAndFoldouts(SerializedProperty prop, Rect position, Rect basePosition, GUIContent label)
		{
			if(prop.hasChildren && prop.isExpanded)
			{
				prop.isExpanded = EditorGUI.PropertyField(position, prop);

				EditorGUI.indentLevel += 1;
				Rect childPosition = basePosition;
				int skipNext = 0;
				foreach(SerializedProperty childProp in prop)
				{
					if(skipNext > 0)
					{
						--skipNext;
						continue;
					}

					childPosition.height = base.GetPropertyHeight(childProp, label);
					childPosition.y += base.GetPropertyHeight(childProp, label) + 2;

					if(childProp.type.Contains("<$"))
					{
						bool hasDrawn = false;
						string typeName = childProp.type.Split('$')[1].Split('>')[0];
						foreach(Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
						{
							System.Type cachedType = assembly.GetType(typeName);
							if(cachedType == null)
								cachedType = assembly.GetType("UnityEngine." + childProp.type.Split('$')[1].Split('>')[0]);
							if(cachedType != null && (cachedType.IsSubclassOf(typeof(Component)) || cachedType == typeof(Component) || cachedType.IsSubclassOf(typeof(NamedMonoBehaviour)) || cachedType == typeof(NamedMonoBehaviour)))
							{
								System.Object targetObject = null;
								bool doubleSize = GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(ComponentSelectionAttribute), false).Length > 0 || GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(NamedMonoBehaviourAttribute), false).Length > 0;
								
								if(doubleSize) childPosition.height *= 2;

								EditorGUI.PropertyField(childPosition, childProp);

								if(doubleSize) childPosition.y += base.GetPropertyHeight(childProp, label);
								hasDrawn = true;
								break;
							}
						}

						if(!hasDrawn)
							EditorGUI.PropertyField(childPosition, childProp);
					}
					else
					{
						System.Object targetObject = null;
						if(childProp.propertyType == SerializedPropertyType.String && GetReflectedFieldInfoRecursively(prop, out targetObject) != null && GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(TextAreaAttribute), false).Length > 0)
						{
							childPosition.height *= 5;
							EditorGUI.PropertyField(childPosition, childProp);
							childPosition.y += base.GetPropertyHeight(childProp, label) * 4;
						}
						else
						{
							EditorGUI.PropertyField(childPosition, childProp);
						}
					}

					if(childProp.propertyType == SerializedPropertyType.Vector2)
						skipNext = 2;
					if(childProp.propertyType == SerializedPropertyType.Vector3)
						skipNext = 3;

#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4
					if(childProp.propertyType == SerializedPropertyType.Rect)
#else
					if(childProp.propertyType == SerializedPropertyType.Vector4 || childProp.propertyType == SerializedPropertyType.Rect)
#endif
						skipNext = 4;
				}
			}
			else
			{
				EditorGUI.PropertyField(position, prop);
			}
		}

		public float GetPropertyHeightIncludingSpecialAndFoldouts(SerializedProperty prop, GUIContent label)
		{
			int skipNext = 0;
			if(prop.hasChildren && prop.isExpanded)
			{
				int i = 1;
				foreach(SerializedProperty childProp in prop)
				{
					if(skipNext > 0)
					{
						--skipNext;
						continue;
					}

					System.Object targetObject = null;
					if(childProp.type.Contains("<$"))
					{
						foreach(Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
						{
							System.Type cachedType = assembly.GetType(childProp.type.Split('$')[1].Split('>')[0]);
							if(cachedType == null)
								cachedType = assembly.GetType("UnityEngine." + childProp.type.Split('$')[1].Split('>')[0]);
							if(cachedType != null && (cachedType.IsSubclassOf(typeof(Component)) || cachedType == typeof(Component) || cachedType.IsSubclassOf(typeof(NamedMonoBehaviour)) || cachedType == typeof(NamedMonoBehaviour)))
							{
								if(GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(ComponentSelectionAttribute), false).Length > 0 || GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(NamedMonoBehaviourAttribute), false).Length > 0)
									++i;
							}
						}
					}
					else if(childProp.propertyType == SerializedPropertyType.String && GetReflectedFieldInfoRecursively(prop, out targetObject) != null && GetReflectedFieldInfoRecursively(prop, out targetObject).GetCustomAttributes(typeof(TextAreaAttribute), false).Length > 0)
					{
						i += 4;
					}

					if(childProp.propertyType == SerializedPropertyType.Vector2)
						skipNext = 2;
					if(childProp.propertyType == SerializedPropertyType.Vector3)
						skipNext = 3;
#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4
					if(childProp.propertyType == SerializedPropertyType.Rect)
#else
					if(childProp.propertyType == SerializedPropertyType.Vector4 || childProp.propertyType == SerializedPropertyType.Rect)
#endif
						skipNext = 4;

					++i;
				}

				return (base.GetPropertyHeight(prop, label) + 1) * i + 2;
			}
			return base.GetPropertyHeight(prop, label);
		}

		public static void CallMethod(SerializedProperty prop, MonoBehaviour go, string methodName)
		{
			MethodInfo buttonFunction = GetMethodRecursively(go.GetType(), methodName);
			if(buttonFunction == null)
			{
				System.Object targetObject = null;
				buttonFunction = GetMethodFromField(prop, methodName, out targetObject);

				if(buttonFunction == null)
				{
					Debug.LogError(string.Format("Function {0} not found in class {1}, its base classes or the current field's class.", methodName, go.GetType().ToString()));
				}
				else
				{
					buttonFunction.Invoke(targetObject, null);
				}
			}
			else
			{
				buttonFunction.Invoke(go, null);
			}
		}

		public static void CallMethod(SerializedProperty prop, ScriptableObject so, string methodName)
		{
			MethodInfo buttonFunction = GetMethodRecursively(so.GetType(), methodName);
			if(buttonFunction == null)
			{
				System.Object targetObject = null;
				buttonFunction = GetMethodFromField(prop, methodName, out targetObject);

				if(buttonFunction == null)
				{
					Debug.LogError(string.Format("Function {0} not found in class {1}, its base classes or the current field's class.", methodName, so.GetType().ToString()));
				}
				else
				{
					buttonFunction.Invoke(targetObject, null);
				}
			}
			else
			{
				buttonFunction.Invoke(so, null);
			}
		}

		public static MethodInfo GetMethodRecursively(System.Type type, string methodName)
		{
			MethodInfo buttonFunction = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public, null, new System.Type[0], null );
			if (buttonFunction == null)
			{
				if(type.BaseType != null)
					return GetMethodRecursively(type.BaseType, methodName);
				else
					return null;
			}
			return buttonFunction;
		}

		public static MethodInfo GetMethodFromField(SerializedProperty prop, string methodName, out System.Object targetObject)
		{
			GetReflectedFieldInfoRecursively(prop, out targetObject);

			if(targetObject != null)
			{
				return targetObject.GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public, null, new System.Type[0], null );
			}
			return null;
		}

		public static T GetReflectedFieldRecursively<T>(SerializedProperty prop, string fieldName = "")
		{
			System.Object targetObject = null;
			FieldInfo info = GetReflectedFieldInfoRecursively(prop, out targetObject, fieldName);

			T temp = default(T);
			try
			{
				temp = (T)(info.GetValue (targetObject));
			}
			catch
			{
				try
				{
					try
					{
						temp = (T)(info.GetValue(targetObject) as IList)[int.Parse(prop.propertyPath.Substring(prop.propertyPath.LastIndexOf("data[") + 5).Split(']')[0])];
					}
					catch
					{
						temp = (T)((System.Object[])info.GetValue(targetObject))[int.Parse(prop.propertyPath.Substring(prop.propertyPath.LastIndexOf("data[") + 5).Split(']')[0])];
					}
				}
				catch
				{
					// Debug.LogWarning(string.Format("The script has no property named {0}.", prop.name));
				}
			}
			
			return temp;
		}

		public static FieldInfo GetReflectedFieldInfoRecursively(SerializedProperty prop, out System.Object targetObject, string fieldName = "")
		{
			string fullpath = prop.propertyPath;

			string propName = prop.name;

			if(!string.IsNullOrEmpty(fieldName))
			{
				fullpath = fullpath.Replace(prop.name, fieldName);
				propName = fieldName;
			}

			List<string> pathList = fullpath.Split ('.').ToList();

			if(propName == "data")
			{
				// This is a list, we must find the real name by getting the string before "Array"

				propName = pathList[pathList.LastIndexOf("Array")-1];
			}

			pathList.Remove("Array");

			targetObject = prop.serializedObject.targetObject;

			if((prop.serializedObject.targetObject as MonoBehaviour) != null)
			{
				FieldInfo temp = (targetObject as MonoBehaviour).GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
				if(temp != null)
				{
					return temp;
				}

				return GetReflectedFieldInfoRecursively(prop.serializedObject.targetObject, (prop.serializedObject.targetObject as MonoBehaviour).GetType().GetField( ( prop.propertyPath.Split ('.').Length == 1 && !string.IsNullOrEmpty(propName) ? propName : prop.propertyPath.Split ('.') [0]), BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathList, out targetObject);
			}

			if((prop.serializedObject.targetObject as ScriptableObject) != null)
			{
				FieldInfo temp = (targetObject as ScriptableObject).GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
				if(temp != null)
				{
					return temp;
				}

				return GetReflectedFieldInfoRecursively(prop.serializedObject.targetObject, (prop.serializedObject.targetObject as ScriptableObject).GetType().GetField(( prop.propertyPath.Split ('.').Length == 1 && !string.IsNullOrEmpty(propName) ? propName : prop.propertyPath.Split ('.') [0]), BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathList, out targetObject);
			}

			return null;
		}

		static FieldInfo GetReflectedFieldInfoRecursively(System.Object targetObject, FieldInfo field, string propName, List<string> pathNames, out System.Object outObject)
		{
			if (pathNames.Count > 1 && pathNames[0] != propName)
			{
				pathNames.RemoveAt(0);

				if(pathNames[0].Contains("data["))
				{
					// The next field is hidden inside a list
					int pathIndex = int.Parse(pathNames[0].Replace("data[", "").Replace("]",""));

					pathNames.RemoveAt(0);

					try
					{
						try
						{
							// If index is greater than length, the list's size is being increased this frame, return null to prevent an error and try again next frame.
							if((field.GetValue(targetObject) as IList).Count <= pathIndex)
							{
								outObject = null;
								return null;
							}
						}
						catch
						{
							if(((System.Object[])field.GetValue(targetObject)).Length <= pathIndex)
							{
								outObject = null;
								return null;
							}
						}
					}
					catch
					{
						// If it breaks here, the value is null. Return for this frame to prevent an error and try again next frame.
						outObject = null;
						return null;
					}

					try
					{
						return GetReflectedFieldInfoRecursively((field.GetValue(targetObject) as IList)[pathIndex], field.FieldType.GetGenericArguments()[0].GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, out outObject);
					}
					catch
					{
						return GetReflectedFieldInfoRecursively(((System.Object[])field.GetValue(targetObject))[pathIndex], field.FieldType.GetElementType().GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, out outObject);
					}
				}
				return GetReflectedFieldInfoRecursively(field.GetValue(targetObject), field.FieldType.GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, out outObject);
			}
			else
			{
				outObject = targetObject;
				return field;
			}
		}

		public static void SetReflectedFieldRecursively<T>(SerializedProperty prop, T value) where T : class
		{
			List<string> pathList = prop.propertyPath.Split ('.').ToList();

			string propName = prop.name;
			
			if(propName == "data")
			{
				// This is a list, we must find the real name by getting the string before "Array"
				
				propName = pathList[pathList.LastIndexOf("Array")-1];
			}

			pathList.Remove("Array");

			if((prop.serializedObject.targetObject as MonoBehaviour) != null)
			{
				foreach (System.Object targetObject in prop.serializedObject.targetObjects)
				{
					SetReflectedFieldRecursively(targetObject, (prop.serializedObject.targetObject as MonoBehaviour).GetType().GetField(prop.propertyPath.Split('.')[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathList, value, prop.propertyPath);
				}
			}
			
			if((prop.serializedObject.targetObject as ScriptableObject) != null)
			{
				foreach (System.Object targetObject in prop.serializedObject.targetObjects)
				{
					SetReflectedFieldRecursively(targetObject, (prop.serializedObject.targetObject as ScriptableObject).GetType().GetField(prop.propertyPath.Split('.')[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathList, value, prop.propertyPath);
				}
			}
		}
		
		static void SetReflectedFieldRecursively<T>(System.Object targetObject, FieldInfo field, string propName, List<string> pathNames, T value, string fullName) where T : class
		{
			if (pathNames.Count >= 1 && pathNames[0] != propName)
			{
				pathNames.RemoveAt(0);

				if(pathNames[0].Contains("data["))
				{
					// The next field is hidden inside a list
					int pathIndex = int.Parse(pathNames[0].Replace("data[", "").Replace("]",""));
					
					pathNames.RemoveAt(0);

					try
					{
						SetReflectedFieldRecursively((field.GetValue(targetObject) as IList)[pathIndex], field.FieldType.GetGenericArguments()[0].GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, value, fullName);
					}
					catch
					{
						SetReflectedFieldRecursively(((System.Object[])field.GetValue(targetObject))[pathIndex], field.FieldType.GetElementType().GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, value, fullName);
					}
				}
				SetReflectedFieldRecursively(field.GetValue(targetObject), field.FieldType.GetField(pathNames[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public), propName, pathNames, value, fullName);
			}
			else
			{
				try
				{
					field.SetValue(targetObject, value);
				}
				catch
				{
					try
					{
						if(field != null)
						{
							try
							{
								(field.GetValue(targetObject) as IList)[int.Parse(fullName.Substring(fullName.LastIndexOf("data[") + 5).Split(']')[0])] = value;
							}
							catch
							{
								((System.Object[])field.GetValue(targetObject))[int.Parse(fullName.Substring(fullName.LastIndexOf("data[") + 5).Split(']')[0])] = value;
							}
						}
					}
					catch
					{
						 //Debug.LogWarning(string.Format("The script has no property named {0}.", propName));
					}
				}
			}
		}
	}
}
