﻿//----------------------------------------------
//            Heavy-Duty Inspector
//      Copyright © 2014 - 2015  Illogika
//----------------------------------------------
#if !UNITY_4_0 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_4
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace HeavyDutyInspector
{

	[CustomPropertyDrawer(typeof(DictionaryAttribute))]
	public class DictionaryDrawer : IllogikaDrawer {
			
		DictionaryAttribute dictionaryAttribute { get { return ((DictionaryAttribute)attribute); } }
		
		public override float GetPropertyHeight (SerializedProperty prop, GUIContent label)
		{
			return base.GetPropertyHeight(prop, label) * (int.Parse(prop.propertyPath.Split(']')[prop.propertyPath.Split(']').Length-2].Split('[').Last()) == 0 ? 2 : 1);
	    }

		private static Texture2D olMinus
		{
			get;
			set;
		}

		private List<string> keywords;

		protected void PopulateList()
		{
			keywords = new List<string>();

			foreach(KeywordCategory category in dictionaryAttribute.keywordConfig.keyWordCategories)
			{
				foreach(string keyword in category.keywords)
				{
					if(!string.IsNullOrEmpty(keyword))
						keywords.Add(category.name + (string.IsNullOrEmpty(category.name) ? "" : "/") + keyword);
				}
			}

			keywords.Sort();
			keywords.Insert(0, "None");
		}

		public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label)
		{
			if(olMinus == null)
				olMinus = (Texture2D)Resources.Load("OLMinusRed");

			if(keywords == null && dictionaryAttribute.keywordConfig != null)
				PopulateList();

			EditorGUI.BeginProperty(position, label, prop);

			int index = int.Parse(prop.propertyPath.Split(']')[prop.propertyPath.Split(']').Length-2].Split('[').Last());

			if(!fieldInfo.FieldType.IsGenericType || fieldInfo.FieldType.IsArray)
			{
				Debug.LogWarning("The Dictionary Attribute can only be used with Lists.");
			}

			IList list1 = null;

			list1 = GetReflectedFieldRecursively<IList>(prop);

			if(list1 == null)
				return;

			IList list2 = null;

			SerializedProperty serializedValues = prop.serializedObject.FindProperty(dictionaryAttribute.valuesListName);
			list2 = GetReflectedFieldRecursively<IList>(serializedValues);
			
			if(list2 == null)
				return;

			System.Type type1 = list1.GetType().GetGenericArguments()[0];

			System.Type type2 = list2.GetType().GetGenericArguments()[0];

			while(list2.Count < list1.Count)
			{
				if(!type2.IsSubclassOf(typeof(Component)) && !type2.IsSubclassOf(typeof(GameObject)))
				{
					list2.Add(System.Activator.CreateInstance(type2));
				}
				else if(type2.IsClass)
				{
					list2.Add(null);
				}
			}

			while(list2.Count > list1.Count)
			{
				list2.RemoveAt(list2.Count - 1);
			}

			position.width -= 16;
			position.width /= 2;

			if(index >= serializedValues.arraySize)
				return;

			if(index == 0)
			{
				position.height /= 2;
				Rect labelPos = position;
				position.y += position.height;
				EditorGUI.LabelField(labelPos, "Key");
				labelPos.x += labelPos.width;
				EditorGUI.LabelField(labelPos, "Value");
			}
			
			if(type1.IsAssignableFrom(typeof(Keyword)) && dictionaryAttribute.keywordConfig != null)
			{
				string temp = prop.FindPropertyRelative("_key").stringValue;

				if(temp == "")
					temp = "None";

				Color originalColor = GUI.color;
				int i = keywords.IndexOf(temp);

				if(i < 0)
				{
					i = keywords.Count;
					keywords.Add(temp + " (Missing)");
					GUI.color = Color.red;
				}

				EditorGUI.BeginChangeCheck();

				i = EditorGUI.Popup(position, i, keywords.ToArray());

				temp = keywords[i];

				if(temp == "None")
					temp = "";

				GUI.color = originalColor;

				if(EditorGUI.EndChangeCheck())
				{
					prop.FindPropertyRelative("_key").stringValue = temp;
				}
			}
			else
			{
				EditorGUI.PropertyField(position, prop, new GUIContent(""));
			}
			position.x += position.width;
			EditorGUI.PropertyField(position, serializedValues.GetArrayElementAtIndex(index), new GUIContent(""));

			position.x += position.width + 4;
			position.width = 16;

			if(GUI.Button(position, olMinus, "Label"))
			{
				list1.RemoveAt(index);
				list2.RemoveAt(index);
			}

			EditorGUI.EndProperty();
		}
	}

}
#endif
