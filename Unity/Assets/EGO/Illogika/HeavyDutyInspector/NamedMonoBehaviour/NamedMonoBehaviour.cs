//----------------------------------------------
//            Heavy-Duty Inspector
//      Copyright © 2013 - 2015  Illogika
//----------------------------------------------

using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using HeavyDutyInspector;

[System.Serializable]
public abstract class NamedMonoBehaviour : MonoBehaviour {

	public NamedMonoBehaviour() : base()
	{
		typeName = GetType().ToString();
	}

	[SerializeField]
	[HideInInspector]
	private string typeName;

	[NMBName]
	public string	scriptName;

	[NMBColor]
	public Color	scriptNameColor = Color.white;

	protected void InitDictionary<T, U>(List<T> keys, List<U> values, Dictionary<T, U> dictionary)
	{
		dictionary = new Dictionary<T, U>();
		for(int i = 0; i < keys.Count; ++i)
		{
			dictionary.Add(keys[i], values[i]);
		}
	} 

	protected void PrepareDictionary<T,U>(Dictionary<T,U> dictionary, List<T> keys, List<U> values)
	{
#if !UNITY_EDITOR
		keys = new List<T>();
		values = new List<U>();
		foreach(KeyValuePair<T,U> kvp in dictionary.ToList())
		{
			keys.Add(kvp.Key);
			values.Add(kvp.Value);
		}
#endif
	}
}