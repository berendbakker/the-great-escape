﻿using UnityEngine;
using System.Collections;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Blocks all game user input")]
	public class BlockAllGameInput : Action
	{
		public SharedBool block = true;

		public override void OnStart()
		{
			if (GameManager.instance!=null) {
				GameManager.instance.BlockAllGameInput(block.Value);
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			return GameManager.instance != null ? TaskStatus.Success : TaskStatus.Failure;
		}
		
		public override void OnReset()
		{
			block = true;
		}
	}
}
