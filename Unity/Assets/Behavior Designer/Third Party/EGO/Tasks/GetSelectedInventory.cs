﻿using UnityEngine;
using System.Collections;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Delete an item from the inventory")]
	public class GetSelectedInventory : Action
	{
		[RequiredField]
		public SharedGameObject storeValue;

		private InventoryItem _itm;
		private InventoryHolder _ih;

		public override void OnStart()
		{
			_ih = GameObject.FindObjectOfType<InventoryHolder> ();
		}

		public override TaskStatus OnUpdate()
		{
			if (_ih!=null) {

				InventoryItem itm = _ih.GetSelected();
				storeValue.Value = null;
				if (itm!=null) {
					storeValue.Value = itm.gameObject;
				}
				return TaskStatus.Success;
			}

			return TaskStatus.Failure;
		}
		

		public override void OnReset()
		{
			storeValue = null;
		}
	}
}
