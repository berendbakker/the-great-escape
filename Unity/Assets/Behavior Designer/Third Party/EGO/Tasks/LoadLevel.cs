﻿using UnityEngine;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Load a level. Empty level name will reload current!")]
	public class LoadLevel : Action
	{
		public SharedString levelName;
		
		public override void OnStart()
		{
			if (string.IsNullOrEmpty(levelName.Value)) {
				
				levelName.Value = Application.loadedLevelName;
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			Application.LoadLevel (levelName.Value);
			return TaskStatus.Success;
		}
		
		public override void OnReset()
		{
			levelName = "";
		}
	}
}
