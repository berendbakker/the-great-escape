﻿using UnityEngine;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Hide all UI directions")]
	public class HideAllDirectionUI : Action
	{
		public SharedGameObject action;

		private UiManager _ui;

		public override void OnStart()
		{
			_ui = GameObject.FindObjectOfType<UiManager> ();
		
		}
		
		public override TaskStatus OnUpdate()
		{
			if (_ui) {
				
				_ui.HideAllDirections();
				return TaskStatus.Success;
			}
			
			return TaskStatus.Failure;
		}
		
		public override void OnReset()
		{
			action = null;
		}
	}
}