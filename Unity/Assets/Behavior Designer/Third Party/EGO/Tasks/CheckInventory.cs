﻿using UnityEngine;
using System.Collections;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Is an item in the inventory")]
	public class CheckInventory : Conditional
	{
		public SharedGameObject inventoryItem;

		private InventoryItem _itm;
		private InventoryHolder _ih;

		public override void OnStart()
		{
			_ih = GameObject.FindObjectOfType<InventoryHolder> ();
			GameObject go = GetDefaultGameObject(inventoryItem.Value);
			if (go != null) {
				_itm = go.GetComponent<InventoryItem>();
			}
		}

		public override TaskStatus OnUpdate()
		{
			if (_ih!=null && _itm!=null) {

				if (_ih.CheckInventory(_itm)) { 

					return TaskStatus.Success;
				}
			}

			return TaskStatus.Failure;
		}
		

		public override void OnReset()
		{
			inventoryItem = null;
		}
	}
}
