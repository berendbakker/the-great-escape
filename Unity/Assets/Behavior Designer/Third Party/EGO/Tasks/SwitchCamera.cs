﻿using UnityEngine;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Switch between two cams with optional cinematic effect.")]
	public class SwitchCamera : Action
	{
		public SharedGameObject cameraTarget;
		public SharedBool mainCamera = false;
		public SharedBool previousCamera = false;
		public SharedFloat transitionTime = 0;
		public SharedBool ignoreTimescale = false;
		public SharedBool waitForCompletion = true;
		
		private Camera cam;
		private CameraManager camMan;
		private bool moveComplete;
		
		public override void OnStart()
		{
			cam = GetDefaultGameObject(cameraTarget.Value).GetComponent<Camera>();
			camMan = GameManager.instance.GetCameraManager ();
			moveComplete = true;
			if (waitForCompletion.Value) {
				moveComplete = false;
			}

			if (mainCamera.Value) {
				camMan.SwitchToMainCam(transitionTime.Value, ignoreTimescale.Value);
			}
			else if (previousCamera.Value) {
				camMan.SwitchToPrevCam(transitionTime.Value, ignoreTimescale.Value);
			}
			else {
				if (cam != null) {
					camMan.SwitchToCam(cam, transitionTime.Value, ignoreTimescale.Value);
				} else {
					Debug.LogError("SwitchCamera error: no target cam found!");
				}
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			if (waitForCompletion.Value && camMan != null) {
				
				moveComplete = !camMan.IsSwitching();
			}
			
			return moveComplete ? TaskStatus.Success : TaskStatus.Running;
		}
		
		public override void OnReset()
		{
			cameraTarget = null;
			mainCamera = false;
			previousCamera = false;
			transitionTime = 0;
			ignoreTimescale = false;
			waitForCompletion = true;
		}
	}
}