﻿using UnityEngine;
using System.Collections;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Starts an Interaction")]
	public class StartInteraction : Action
	{
		public SharedGameObject source;
		public SharedVector3 position;
		public SharedGameObject interaction;
		public SharedBool waitForCompletion = true;
		
		private ObjectInteraction _oi;
		private GameObject _source;
		private bool _waitComplete = false;


		public override void OnStart()
		{
			_source = GetDefaultGameObject(source.Value);

			_oi = null;
			if (interaction.Value!=null) {
				_oi = interaction.Value.GetComponent<ObjectInteraction>();
			}

			_waitComplete = true;
			if (_oi != null) {
				if (waitForCompletion.Value) {
					_waitComplete = false;
				}
				_oi.Interact(_source, position.Value);
			} else {
				Debug.LogError("Interaction error: Interaction not found!");
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			if (waitForCompletion.Value && _oi != null) {
				
				_waitComplete = !_oi.IsBusy();
			}
			
			return _waitComplete ? TaskStatus.Success : TaskStatus.Running;
		}
		
		public override void OnReset()
		{
			source = null;
			position = null;
			interaction = null;
			waitForCompletion = true;
		}
	}
}
