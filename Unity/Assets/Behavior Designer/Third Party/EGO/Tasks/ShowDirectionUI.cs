﻿using UnityEngine;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Activate UI directional button")]
	public class ShowDirectionUI : Action
	{
		public SharedGameObject bottomActionBT;
		public SharedGameObject topActionBT;
		public SharedGameObject leftActionBT;
		public SharedGameObject rightActionBT;

		private GameObject _action;
		private UiManager _ui;

		public override void OnStart()
		{

			bottomActionBT = bottomActionBT.Value;
			topActionBT = topActionBT.Value;
			leftActionBT = leftActionBT.Value;
			rightActionBT = rightActionBT.Value;

			_ui = GameObject.FindObjectOfType<UiManager> ();
		
		}

		public override TaskStatus OnUpdate()
		{
			TaskStatus ts = TaskStatus.Failure;

			if (_ui!=null) {

				if (bottomActionBT.Value!=null) {

					_action = bottomActionBT.Value;
					BehaviorTree bt = _action.GetComponent<BehaviorTree> ();
					if (bt != null) {
						
						_ui.ShowDirectionBottom(bt);
						ts = TaskStatus.Success;
					}
					else {
						ObjectInteraction oi = _action.GetComponent<ObjectInteraction> ();
						if (oi != null) {
							
							_ui.ShowDirectionBottom(oi);
							ts = TaskStatus.Success;
						}
					}
				}
				if (topActionBT.Value!=null) {
					
					_action = topActionBT.Value;
					BehaviorTree bt = _action.GetComponent<BehaviorTree> ();
					if (bt != null) {
						
						_ui.ShowDirectionTop(bt);
						ts = TaskStatus.Success;
					}
					else {
						ObjectInteraction oi = _action.GetComponent<ObjectInteraction> ();
						if (oi != null) {
							
							_ui.ShowDirectionTop(oi);
							ts = TaskStatus.Success;
						}
					}
				}
				if (leftActionBT.Value!=null) {
					
					_action = leftActionBT.Value;
					BehaviorTree bt = _action.GetComponent<BehaviorTree> ();
					if (bt != null) {
						
						_ui.ShowDirectionLeft(bt);
						ts = TaskStatus.Success;
					}
					else {
						ObjectInteraction oi = _action.GetComponent<ObjectInteraction> ();
						if (oi != null) {
							
							_ui.ShowDirectionLeft(oi);
							ts = TaskStatus.Success;
						}
					}
				}
				if (rightActionBT.Value!=null) {
					
					_action = rightActionBT.Value;
					BehaviorTree bt = _action.GetComponent<BehaviorTree> ();
					if (bt != null) {
						
						_ui.ShowDirectionRight(bt);
						ts = TaskStatus.Success;
					}
					else {
						ObjectInteraction oi = _action.GetComponent<ObjectInteraction> ();
						if (oi != null) {
							
							_ui.ShowDirectionRight(oi);
							ts = TaskStatus.Success;
						}
					}
				}

			}
			
			return ts;
		}
		
		public override void OnReset()
		{
			bottomActionBT = null;
			topActionBT = null;
			leftActionBT = null;
			rightActionBT = null;
		}
	}
}