﻿using UnityEngine;
using System.Collections;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Stops an Interaction")]
	public class StopInteraction : Action
	{
		public SharedGameObject interaction;

		private ObjectInteraction _oi;

		
		public override void OnStart()
		{
			_oi = null;
			if (interaction.Value!=null) {
				_oi = interaction.Value.GetComponent<ObjectInteraction>();
			}
			if (_oi != null) {

				_oi.StopInteraction();
			} else {
				Debug.LogError("Interaction error: Interaction not found!");
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			return TaskStatus.Success;
		}
		
		public override void OnReset()
		{
			interaction = null;
		}
	}
}
