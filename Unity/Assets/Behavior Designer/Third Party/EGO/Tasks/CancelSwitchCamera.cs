﻿using UnityEngine;
using EGO;

namespace BehaviorDesigner.Runtime.Tasks.EGO
{
	[TaskCategory("EGO")]
	[TaskDescription("Cancel switch between cams.")]
	public class CancelSwitchCamera : Action
	{

		private CameraManager camMan;

		public override void OnStart()
		{
			camMan = GameManager.instance.GetCameraManager ();

			if (camMan != null) {
				camMan.CancelSwitch();
			} else {
				Debug.LogError("CancelSwitchCamera error: no CameraManager found!");
			}
		}
		
		public override TaskStatus OnUpdate()
		{
			return camMan!=null ? TaskStatus.Success : TaskStatus.Failure;
		}
		
		public override void OnReset()
		{
		}
	}
}